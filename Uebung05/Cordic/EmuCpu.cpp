#include "EmuCpu.h"
#include "ResetGenerator.h"

SC_HAS_PROCESS(EmuCpu);

std::unique_ptr<EmuCpu> EmuCpu::instance = nullptr;

extern "C"
{
	void write_bus(unsigned int adr, unsigned int data)
	{
		EmuCpu::getInstance().write_bus(adr, data);
	}

	void read_bus(unsigned int adr, unsigned int* data)
	{
		EmuCpu::getInstance().read_bus(adr, *data);
	}
}

EmuCpu::EmuCpu(sc_module_name name, int (* main)()): sc_module(name)
{
	main_c = main;
	SC_THREAD(run);
}

void EmuCpu::run()
{
	wait(sc_time(ResetGenerator::NSUntilReset, SC_NS));
	main_c();
	sc_stop();
}

void EmuCpu::doTransaction(tlm::tlm_command cmd, unsigned adr, unsigned& data)
{
	sc_time delay = sc_time(0, SC_NS);

	tlm::tlm_generic_payload* trans = new tlm::tlm_generic_payload;

	trans->set_data_length(4);
	trans->set_streaming_width(4);
	trans->set_byte_enable_length(0);
	trans->set_byte_enable_ptr(0);
	trans->set_dmi_allowed(false);
	trans->set_response_status(tlm::TLM_INCOMPLETE_RESPONSE);

	//set specific parameters
	trans->set_command(cmd);
	trans->set_address(adr);
	auto ptr = &data;
	trans->set_data_ptr(reinterpret_cast<unsigned char*>(&data));

	socket->b_transport(*trans, delay);

	if (trans->is_response_error())
	{
		char txt[100];
		sprintf(txt, "Error from b_transport, response status = %s",
		        trans->get_response_string().c_str());
		SC_REPORT_ERROR("TLM-2", "Response error from b_transport");
	}
	
	delete trans;
}

EmuCpu& EmuCpu::getInstance(char const* name, int (* main)())
{
	if (instance == nullptr)
	{
		instance = std::unique_ptr<EmuCpu>(new EmuCpu(name, main));
	}

	return *instance;
}

EmuCpu& EmuCpu::getInstance()
{
	return *instance;
}

void EmuCpu::write_bus(unsigned adr, unsigned data)
{
	doTransaction(tlm::TLM_WRITE_COMMAND, adr, data);
	wait(1, SC_NS);
}

void EmuCpu::read_bus(unsigned adr, unsigned& data)
{
	doTransaction(tlm::TLM_READ_COMMAND, adr, data);
	wait(1, SC_NS);
}
