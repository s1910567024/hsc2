#pragma once

#include "mySystemc.h"

SC_MODULE(ResetGenerator)
{
	sc_out<bool> rst;
	
	static const int NSUntilReset{ 40 };

	SC_CTOR(ResetGenerator)
	{
		SC_THREAD(doWork);
	}

private:
	void doWork()
	{
		rst = true;
		wait(NSUntilReset, SC_NS);
		rst = false;
	}
};
