############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2019 Xilinx, Inc. All Rights Reserved.
############################################################
open_project hls_cordic
set_top cordiccc
add_files Cordic/cordiccc.cpp
add_files -tb Cordic/Cordic.cpp -cflags "-DEMUCPUINSTANCE -Wno-unknown-pragmas"
add_files -tb Cordic/EmuCpu.cpp -cflags "-DEMUCPUINSTANCE -Wno-unknown-pragmas"
add_files -tb Cordic/cordic_drv.c -cflags "-DEMUCPUINSTANCE -Wno-unknown-pragmas"
add_files -tb Cordic/hal.c -cflags "-DEMUCPUINSTANCE -Wno-unknown-pragmas"
add_files -tb Cordic/main.cpp -cflags "-DEMUCPUINSTANCE -Wno-unknown-pragmas"
add_files -tb Cordic/main_c.c -cflags "-DEMUCPUINSTANCE -Wno-unknown-pragmas"
add_files -tb Cordic/xcordiccc_sinit.c -cflags "-DEMUCPUINSTANCE -Wno-unknown-pragmas"
add_files -tb Cordic/xscugic.c -cflags "-DEMUCPUINSTANCE -Wno-unknown-pragmas"
open_solution "solution1"
set_part {xc7z007s-clg225-1} -tool vivado
create_clock -period 10 -name default
config_export -format ip_catalog -ipname cordiccc_top -rtl verilog -use_netlist none -vivado_optimization_level 2 -vivado_phys_opt place -vivado_report_level 0
#source "./hls_cordic/solution1/directives.tcl"
csim_design
csynth_design
cosim_design
export_design -rtl verilog -format ip_catalog
