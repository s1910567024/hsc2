// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Mon Jul  6 15:33:32 2020
// Host        : DESKTOP-5PTVA4F running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ uebung08_cordiccc_top_0_0_stub.v
// Design      : uebung08_cordiccc_top_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z007sclg225-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "cordiccc_top,Vivado 2019.1" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(s_axi_cordic_if_AWADDR, 
  s_axi_cordic_if_AWVALID, s_axi_cordic_if_AWREADY, s_axi_cordic_if_WDATA, 
  s_axi_cordic_if_WSTRB, s_axi_cordic_if_WVALID, s_axi_cordic_if_WREADY, 
  s_axi_cordic_if_BRESP, s_axi_cordic_if_BVALID, s_axi_cordic_if_BREADY, 
  s_axi_cordic_if_ARADDR, s_axi_cordic_if_ARVALID, s_axi_cordic_if_ARREADY, 
  s_axi_cordic_if_RDATA, s_axi_cordic_if_RRESP, s_axi_cordic_if_RVALID, 
  s_axi_cordic_if_RREADY, aclk, aresetn)
/* synthesis syn_black_box black_box_pad_pin="s_axi_cordic_if_AWADDR[5:0],s_axi_cordic_if_AWVALID,s_axi_cordic_if_AWREADY,s_axi_cordic_if_WDATA[31:0],s_axi_cordic_if_WSTRB[3:0],s_axi_cordic_if_WVALID,s_axi_cordic_if_WREADY,s_axi_cordic_if_BRESP[1:0],s_axi_cordic_if_BVALID,s_axi_cordic_if_BREADY,s_axi_cordic_if_ARADDR[5:0],s_axi_cordic_if_ARVALID,s_axi_cordic_if_ARREADY,s_axi_cordic_if_RDATA[31:0],s_axi_cordic_if_RRESP[1:0],s_axi_cordic_if_RVALID,s_axi_cordic_if_RREADY,aclk,aresetn" */;
  input [5:0]s_axi_cordic_if_AWADDR;
  input s_axi_cordic_if_AWVALID;
  output s_axi_cordic_if_AWREADY;
  input [31:0]s_axi_cordic_if_WDATA;
  input [3:0]s_axi_cordic_if_WSTRB;
  input s_axi_cordic_if_WVALID;
  output s_axi_cordic_if_WREADY;
  output [1:0]s_axi_cordic_if_BRESP;
  output s_axi_cordic_if_BVALID;
  input s_axi_cordic_if_BREADY;
  input [5:0]s_axi_cordic_if_ARADDR;
  input s_axi_cordic_if_ARVALID;
  output s_axi_cordic_if_ARREADY;
  output [31:0]s_axi_cordic_if_RDATA;
  output [1:0]s_axi_cordic_if_RRESP;
  output s_axi_cordic_if_RVALID;
  input s_axi_cordic_if_RREADY;
  input aclk;
  input aresetn;
endmodule
