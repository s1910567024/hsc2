#pragma once

#include "hal.h"

CordicStatus_t CordicCalcXY(float const* const phi, float* const cos, float* const sin, unsigned int* adr);

CordicStatus_t cordic_init();
