#pragma once

typedef enum
{
	Cordic_Status_OK,
	Cordic_Status_InitializationError
} CordicStatus_t;

CordicStatus_t CordicCalcXY(float const* const phi, float* const cos, float* const sin, unsigned int* adr);

CordicStatus_t cordic_init();