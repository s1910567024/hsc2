# ==============================================================
# Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
# Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
# ==============================================================
CSIM_DESIGN = 1

__SIM_FPO__ = 1

__SIM_MATHHLS__ = 1

__SIM_OPENCV__ = 1

__SIM_FFT__ = 1

__SIM_FIR__ = 1

__SIM_DDS__ = 1

ObjDir = obj

HLS_SOURCES = ../../../../Cordic/Cordic.cpp ../../../../Cordic/EmuCpu.cpp ../../../../Cordic/cordic_drv.c ../../../../Cordic/hal.c ../../../../Cordic/main.cpp ../../../../Cordic/main_c.c ../../../../Cordic/xcordic_sinit.c ../../../../Cordic/CordicCC.cpp

TARGET := csim.exe

AUTOPILOT_ROOT := B:/Programme/Xilinx/Vivado/2019.1
AUTOPILOT_MACH := win64
ifdef AP_GCC_M32
  AUTOPILOT_MACH := Linux_x86
  IFLAG += -m32
endif
ifndef AP_GCC_PATH
  AP_GCC_PATH := B:/Programme/Xilinx/Vivado/2019.1/msys64/mingw64/bin
endif
AUTOPILOT_TOOL := ${AUTOPILOT_ROOT}/${AUTOPILOT_MACH}/tools
AP_CLANG_PATH := ${AUTOPILOT_ROOT}/msys64/mingw64/bin
AUTOPILOT_TECH := ${AUTOPILOT_ROOT}/common/technology


IFLAG += -I "${AUTOPILOT_TOOL}/systemc/include"
IFLAG += -I "${AUTOPILOT_ROOT}/include"
IFLAG += -I "${AUTOPILOT_ROOT}/include/opencv"
IFLAG += -I "${AUTOPILOT_ROOT}/include/ap_sysc"
IFLAG += -I "${AUTOPILOT_TECH}/generic/SystemC"
IFLAG += -I "${AUTOPILOT_TECH}/generic/SystemC/AESL_FP_comp"
IFLAG += -I "${AUTOPILOT_TECH}/generic/SystemC/AESL_comp"
IFLAG += -I "${AUTOPILOT_TOOL}/auto_cc/include"
IFLAG += -D__SIM_FPO__

IFLAG += -D__SIM_OPENCV__

IFLAG += -D__SIM_FFT__

IFLAG += -D__SIM_FIR__

IFLAG += -D__SIM_DDS__

IFLAG += -D__DSP48E1__
LFLAG += -L$(AUTOPILOT_TOOL)/systemc/lib -lsystemc
IFLAG += -g
IFLAG += -DNT
LFLAG += -Wl,--enable-auto-import 
DFLAG += -DAUTOCC
DFLAG += -D__xilinx_ip_top= -DAESL_TB
CCFLAG += 
TOOLCHAIN += 



include ./Makefile.rules

all: $(TARGET)



AUTOCC := cmd //c apcc.bat  

$(ObjDir)/Cordic.o: ../../../../Cordic/Cordic.cpp $(ObjDir)/.dir
	$(Echo) "   Compiling ../../../../Cordic/Cordic.cpp in $(BuildMode) mode" $(AVE_DIR_DLOG)
	$(Verb)  $(CC) ${CCFLAG} -c -MMD -Wno-unknown-pragmas -Wno-unknown-pragmas  $(IFLAG) $(DFLAG) $< -o $@ ; \

-include $(ObjDir)/Cordic.d

$(ObjDir)/EmuCpu.o: ../../../../Cordic/EmuCpu.cpp $(ObjDir)/.dir
	$(Echo) "   Compiling ../../../../Cordic/EmuCpu.cpp in $(BuildMode) mode" $(AVE_DIR_DLOG)
	$(Verb)  $(CC) ${CCFLAG} -c -MMD -Wno-unknown-pragmas -Wno-unknown-pragmas  $(IFLAG) $(DFLAG) $< -o $@ ; \

-include $(ObjDir)/EmuCpu.d

$(ObjDir)/cordic_drv.o: ../../../../Cordic/cordic_drv.c $(ObjDir)/.dir
	$(Echo) "   Compiling(apcc) ../../../../Cordic/cordic_drv.c in $(BuildMode) mode" $(AVE_DIR_DLOG)
	$(Verb)  $(AUTOCC) -c -MMD -DEMUCPUINSTANCE -Wno-unknown-pragmas -Wno-unknown-pragmas  $(IFLAG) $(DFLAG) $< -o $@ ; \

-include $(ObjDir)/cordic_drv.d

$(ObjDir)/hal.o: ../../../../Cordic/hal.c $(ObjDir)/.dir
	$(Echo) "   Compiling(apcc) ../../../../Cordic/hal.c in $(BuildMode) mode" $(AVE_DIR_DLOG)
	$(Verb)  $(AUTOCC) -c -MMD -DEMUCPUINSTANCE -Wno-unknown-pragmas -Wno-unknown-pragmas  $(IFLAG) $(DFLAG) $< -o $@ ; \

-include $(ObjDir)/hal.d

$(ObjDir)/main.o: ../../../../Cordic/main.cpp $(ObjDir)/.dir
	$(Echo) "   Compiling ../../../../Cordic/main.cpp in $(BuildMode) mode" $(AVE_DIR_DLOG)
	$(Verb)  $(CC) ${CCFLAG} -c -MMD -Wno-unknown-pragmas -Wno-unknown-pragmas  $(IFLAG) $(DFLAG) $< -o $@ ; \

-include $(ObjDir)/main.d

$(ObjDir)/main_c.o: ../../../../Cordic/main_c.c $(ObjDir)/.dir
	$(Echo) "   Compiling(apcc) ../../../../Cordic/main_c.c in $(BuildMode) mode" $(AVE_DIR_DLOG)
	$(Verb)  $(AUTOCC) -c -MMD -Wno-unknown-pragmas -Wno-unknown-pragmas  $(IFLAG) $(DFLAG) $< -o $@ ; \

-include $(ObjDir)/main_c.d

$(ObjDir)/xcordic_sinit.o: ../../../../Cordic/xcordic_sinit.c $(ObjDir)/.dir
	$(Echo) "   Compiling(apcc) ../../../../Cordic/xcordic_sinit.c in $(BuildMode) mode" $(AVE_DIR_DLOG)
	$(Verb)  $(AUTOCC) -c -MMD -Wno-unknown-pragmas -Wno-unknown-pragmas  $(IFLAG) $(DFLAG) $< -o $@ ; \

-include $(ObjDir)/xcordic_sinit.d

$(ObjDir)/CordicCC.o: ../../../../Cordic/CordicCC.cpp $(ObjDir)/.dir
	$(Echo) "   Compiling ../../../../Cordic/CordicCC.cpp in $(BuildMode) mode" $(AVE_DIR_DLOG)
	$(Verb)  $(CC) ${CCFLAG} -c -MMD -DEMUCPUINSTANCE -DEMUCPUINSTANCE  $(IFLAG) $(DFLAG) $< -o $@ ; \

-include $(ObjDir)/CordicCC.d
