#include "main_c.h"
#include "cordic_drv.h"
#include <stdio.h>
#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"

#define M_PI 3.14159265358979323846

int main()
{
	float phi = 0;
	float cos;
	float sin;
	unsigned int adr = 0;

    init_platform();
	cordic_init();

	for(float i = 0; i <= 360; i+=0.0125f)
	{
		phi = i / 180 * M_PI;

		CordicCalcXY(&phi, &cos, &sin, &adr);
		printf("phi: %f\ncos: %f\nsin: %f\n\n", phi, cos, sin);
		fflush(stdout);
	}

    cleanup_platform();
    return 0;
}
