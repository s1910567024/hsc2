#pragma once

typedef void (*Xil_ExceptionHandler)(void *data);
typedef void (*Xil_InterruptHandler)(void *data);
#define XSCUGIC_MAX_NUM_INTR_INPUTS    	95U

typedef struct
{
	Xil_InterruptHandler Handler;
	void *CallBackRef;
} XScuGic_VectorTableEntry;

typedef struct
{
	unsigned short DeviceId;		/**< Unique ID  of device */
	unsigned int CpuBaseAddress;	/**< CPU Interface Register base address */
	unsigned int DistBaseAddress;	/**< Distributor Register base address */
	XScuGic_VectorTableEntry HandlerTable[XSCUGIC_MAX_NUM_INTR_INPUTS];
} XScuGic_Config;

typedef struct
{
	XScuGic_Config *Config;  /**< Configuration table entry */
	unsigned int IsReady;		 /**< Device is initialized and ready */
	unsigned int UnhandledInterrupts; /**< Intc Statistics */
} XScuGic;


void Xil_ExceptionEnable();

void XScuGic_Enable(XScuGic* InstancePtr, unsigned int Int_Id);

int XScuGic_Connect(XScuGic *InstancePtr, unsigned int Int_Id, Xil_InterruptHandler Handler, void *CallBackRef);

void Xil_ExceptionRegisterHandler(unsigned int Exception_id, Xil_ExceptionHandler Handler, void* Data);

void Xil_ExceptionInit();

int XScuGic_CfgInitialize(XScuGic *InstancePtr, XScuGic_Config *ConfigPtr, unsigned int EffectiveAddr);

XScuGic_Config *XScuGic_LookupConfig(unsigned short DeviceId);

void XScuGic_InterruptHandler(XScuGic *InstancePtr);
