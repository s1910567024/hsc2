#include "cordic_drv.h"
#include "hal.h"
#include <math.h>
#include <stdbool.h>
#include "xstatus.h"
#include "xscugic.h"
#include "xparameters.h"
#include "xcordic.h"
#include "xil_exception.h"
#include <stdint.h>

#define M_PI 3.14159265358979323846

static bool rdy_irq = false;

#define INTC_DEVICE_ID			XPAR_SCUGIC_SINGLE_DEVICE_ID
#define CORDIC_DEVICE_ID		XPAR_AXI_CORDIC_0_DEVICE_ID
#define CORDIC_IRPT_INTR		XPAR_CORDIC_INTR

static XScuGic IntcInstance;
static XCordic CordicInstance;

CordicStatus_t CordicCalcXY(float const* const phi, float* const cos, float* const sin, unsigned int* adr)
{
	float bPhi = fmod(*phi, 2*M_PI); //Between -2pi and 2*pi
	if (bPhi < 0)
		bPhi += 2 * M_PI;//Between 0 and 2*pi

	unsigned int data = (unsigned int)(fmod(bPhi, M_PI/2) * (1<<21));

	rdy_irq = false;

	CordicWrPhi(*adr, data);

	while (!rdy_irq)
	{
#ifdef EMUCPUINSTANCE
		WaitForIrq();
		rdy_irq = true;
#endif
	}

	data = CordicRdXY(*adr);

	*cos = (data >> 16) * 1.0 / (1 << 16);
	*sin = (data & ((1 << 16) - 1)) * 1.0 / (1 << 16);

	if(bPhi < M_PI / 2) //1.Quadrant
	{
	}
	else if(bPhi < 2* M_PI / 2) //2.Quadrant
	{
		float tmp = *cos;
		*cos = -*sin;
		*sin = tmp;
	}
	else if (bPhi < 3 * M_PI / 2) //3.Quadrant
	{
		*cos = -*cos;
		*sin = -*sin;
	}
	else //4.Quadrant
	{
		float tmp = *sin;
		*sin = -*cos;
		*cos = tmp;
	}

	return Cordic_Status_OK;
}

static void CordicIntrHandler(void *CallBackRef)
{
		rdy_irq = true;
}

CordicStatus_t cordic_init()
{
	int Status;
	
	XCordic_Initialize(&CordicInstance, CORDIC_DEVICE_ID);

	XScuGic_Config *IntcConfig;

	/*
	 * Initialize the interrupt controller driver so that it is ready to
	 * use.
	 */
	IntcConfig = XScuGic_LookupConfig(INTC_DEVICE_ID);
	if (NULL == IntcConfig) {
		return Cordic_Status_InitializationError;
	}

	Status = XScuGic_CfgInitialize(&IntcInstance, IntcConfig,	IntcConfig->CpuBaseAddress);
	if (Status != XST_SUCCESS) {
		return Cordic_Status_InitializationError;
	}

	Xil_ExceptionInit();

	/*
	 * Connect the interrupt controller interrupt handler to the hardware
	 * interrupt handling logic in the processor.
	 */
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_IRQ_INT, (Xil_ExceptionHandler)XScuGic_InterruptHandler, &IntcInstance);

	/*
	 * Connect the device driver handler that will be called when an
	 * interrupt for the device occurs, the handler defined above performs
	 * the specific interrupt processing for the device.
	 */
	Status = XScuGic_Connect(&IntcInstance, CORDIC_IRPT_INTR, (Xil_ExceptionHandler)CordicIntrHandler, (void *)&CordicInstance);
	if (Status != XST_SUCCESS) {
		return Cordic_Status_InitializationError;
	}

	/*
	 * Enable the interrupt for the device.
	 */
	XScuGic_Enable(&IntcInstance, CORDIC_IRPT_INTR);

	/*
	 * Enable the timer interrupts for timer mode.
	 */
	XCordic_InterruptEnable(&CordicInstance);

	/*
	 * Enable interrupts in the Processor.
	 */
	Xil_ExceptionEnable();

	return Cordic_Status_OK;
}