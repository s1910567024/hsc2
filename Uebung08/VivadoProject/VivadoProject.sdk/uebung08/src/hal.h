#pragma once

typedef enum
{
	Cordic_Status_OK,
	Cordic_Status_InitializationError
} CordicStatus_t;

unsigned int CordicRdCtl(unsigned int adr);

void CordicWrPhi(unsigned int adr, unsigned int data);

unsigned int CordicRdXY(unsigned int adr);

CordicStatus_t hal_cordic_init();

void WaitForIrq();
