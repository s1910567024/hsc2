#pragma once

#include <systemc.h>
#include <random>

SC_MODULE(MemoryCC)
{
	sc_in<bool> iClk, iRst;
	sc_in<int> iAdr;
	sc_in<int> iDat;
	sc_in<int> iSel;
	sc_in<bool> iCyc, iStb, iWe;
	sc_out<int> oDat;
	sc_out<bool> oAck;


	SC_CTOR(MemoryCC)
	{
		SC_THREAD(doWork);
	}

private:
	enum { SIZE = 256 };

	int mMem[SIZE];

	std::mt19937 mEngine{0};
	std::uniform_int_distribution<> mDist{ 1, 10 };

	void doWork()
	{
		while (true)
		{
			wait(iClk.posedge_event() | iRst.value_changed_event());

			if (iRst == true)
			{
				oAck = false;
				oDat = 0;
			}
			else
			{
				oAck = false;

				if (iCyc == true && iStb == true)
				{
					int addrLen = iAdr / 4;
					if (addrLen >= sc_dt::uint64(SIZE)) {
						std::cout << "FATAL: Address errror" << std::endl;
						return;
					}

					int ws = mDist(mEngine);
					for (int i = 0; i < ws; ++i)
						wait(iClk.posedge_event());

					oAck = true;
					if (iWe == false) //read
					{
						oDat = mMem[addrLen];
						wait(iClk.posedge_event());
						oDat = 0;
					}
					else //write
					{
						wait(iClk.posedge_event());
						mMem[addrLen] = iDat;
					}

					oAck = false;
				}
			}
		}
	}
};