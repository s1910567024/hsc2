/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "gpios.h"
#include "I2C.h"
#include "timer.h"

#define TIMER_FREQUENCY 111111115

int counter = 0;

void timerCallback(void)
{
	++counter;

	switch(counter)
	{
	case 0:
		set_PL_LED_color(LED_COLOR_OFF);
		set_PS_LED_color(LED_COLOR_OFF);
		break;
	case 1:
		set_PL_LED_color(LED_COLOR_GREEN);
		set_PS_LED_color(LED_COLOR_OFF);
		break;
	case 2:
		set_PL_LED_color(LED_COLOR_OFF);
		set_PS_LED_color(LED_COLOR_GREEN);
		break;
	case 3:
		set_PL_LED_color(LED_COLOR_GREEN);
		set_PS_LED_color(LED_COLOR_GREEN);
		counter = -1;
		break;
	}
}

int main()
{
    init_platform();

    init_PS_led();
    init_PL_led(GPIO_AXI0_DEVICE_ID);

    sensor_init();
    timer_init();

    timer_setCallback(&timerCallback);
    timer_setLoadValue(TIMER_FREQUENCY*2);

    timer_start();

	set_PL_LED_color(LED_COLOR_OFF);
	set_PS_LED_color(LED_COLOR_OFF);

	while (1)
	{
		read_temperature();
		read_motion();
		fflush(stdout);
		//printf("\r\n"); //Blank line
		sleep(2); //seconds
	}

    cleanup_platform();
    return 0;
}
