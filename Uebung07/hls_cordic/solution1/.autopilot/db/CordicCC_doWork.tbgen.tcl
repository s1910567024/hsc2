set moduleName CordicCC_doWork
set isTopModule 0
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {CordicCC::doWork}
set C_modelType { void 0 }
set C_modelArgList {
	{ iStart int 1 regular {pointer 0 volatile }  }
	{ oRdy int 1 regular {pointer 1 volatile }  }
	{ iPhi int 22 regular {pointer 0 volatile }  }
	{ oX int 16 regular {pointer 1 volatile }  }
	{ oY int 16 regular {pointer 1 volatile }  }
}
set C_modelArgMapList {[ 
	{ "Name" : "iStart", "interface" : "wire", "bitwidth" : 1, "direction" : "READONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "CordicCC.iStart.m_if.Val","cData": "bool","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "oRdy", "interface" : "wire", "bitwidth" : 1, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "CordicCC.oRdy.m_if.Val","cData": "bool","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "iPhi", "interface" : "wire", "bitwidth" : 22, "direction" : "READONLY", "bitSlice":[{"low":0,"up":21,"cElement": [{"cName": "CordicCC.iPhi.m_if.Val.V","cData": "uint22","bit_use": { "low": 0,"up": 21},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "oX", "interface" : "wire", "bitwidth" : 16, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":15,"cElement": [{"cName": "CordicCC.oX.m_if.Val.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "oY", "interface" : "wire", "bitwidth" : 16, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":15,"cElement": [{"cName": "CordicCC.oY.m_if.Val.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} ]}
# RTL Port declarations: 
set portNum 10
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_low_sync } 
	{ iStart sc_in sc_logic 1 signal 0 } 
	{ oRdy sc_out sc_logic 1 signal 1 } 
	{ oRdy_ap_vld sc_out sc_logic 1 outvld 1 } 
	{ iPhi sc_in sc_lv 22 signal 2 } 
	{ oX sc_out sc_lv 16 signal 3 } 
	{ oX_ap_vld sc_out sc_logic 1 outvld 3 } 
	{ oY sc_out sc_lv 16 signal 4 } 
	{ oY_ap_vld sc_out sc_logic 1 outvld 4 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "iStart", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "iStart", "role": "default" }} , 
 	{ "name": "oRdy", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "oRdy", "role": "default" }} , 
 	{ "name": "oRdy_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "oRdy", "role": "ap_vld" }} , 
 	{ "name": "iPhi", "direction": "in", "datatype": "sc_lv", "bitwidth":22, "type": "signal", "bundle":{"name": "iPhi", "role": "default" }} , 
 	{ "name": "oX", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "oX", "role": "default" }} , 
 	{ "name": "oX_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "oX", "role": "ap_vld" }} , 
 	{ "name": "oY", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "oY", "role": "default" }} , 
 	{ "name": "oY_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "oY", "role": "ap_vld" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1", "2", "3"],
		"CDFG" : "CordicCC_doWork",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "0", "ap_start" : "0", "ap_ready" : "0", "ap_done" : "0", "ap_continue" : "0", "ap_idle" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "137", "EstimateLatencyMax" : "137",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"Port" : [
			{"Name" : "clk", "Type" : "None", "Direction" : "I"},
			{"Name" : "nrst", "Type" : "None", "Direction" : "I"},
			{"Name" : "iStart", "Type" : "None", "Direction" : "I"},
			{"Name" : "oRdy", "Type" : "Vld", "Direction" : "O"},
			{"Name" : "iPhi", "Type" : "None", "Direction" : "I"},
			{"Name" : "oX", "Type" : "Vld", "Direction" : "O"},
			{"Name" : "oY", "Type" : "Vld", "Direction" : "O"},
			{"Name" : "atanArray_V", "Type" : "Memory", "Direction" : "I"}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.atanArray_V_U", "Parent" : "0"},
	{"ID" : "2", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.CordicCC_mul_mul_cud_U1", "Parent" : "0"},
	{"ID" : "3", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.CordicCC_mul_mul_cud_U2", "Parent" : "0"}]}


set ArgLastReadFirstWriteLatency {
	CordicCC_doWork {
		clk {Type I LastRead -1 FirstWrite -1}
		nrst {Type I LastRead -1 FirstWrite -1}
		iStart {Type I LastRead 2 FirstWrite -1}
		oRdy {Type O LastRead -1 FirstWrite 0}
		iPhi {Type I LastRead 2 FirstWrite -1}
		oX {Type O LastRead -1 FirstWrite 6}
		oY {Type O LastRead -1 FirstWrite 8}
		atanArray_V {Type I LastRead -1 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "137", "Max" : "137"}
	, {"Name" : "Interval", "Min" : "137", "Max" : "137"}
]}

set PipelineEnableSignalInfo {[
]}

set Spec2ImplPortList { 
	iStart { ap_none {  { iStart in_data 0 1 } } }
	oRdy { ap_vld {  { oRdy out_data 1 1 }  { oRdy_ap_vld out_vld 1 1 } } }
	iPhi { ap_none {  { iPhi in_data 0 22 } } }
	oX { ap_vld {  { oX out_data 1 16 }  { oX_ap_vld out_vld 1 1 } } }
	oY { ap_vld {  { oY out_data 1 16 }  { oY_ap_vld out_vld 1 1 } } }
}
