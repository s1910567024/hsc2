#pragma once

#include "mySystemc.h"

SC_MODULE(CordicCC)
{
	sc_in<bool> clk;
	sc_in<bool> nrst;
	sc_in<bool> iStart;
	sc_out<bool> oRdy;
	sc_in<sc_ufixed<22, 1>> iPhi;
	sc_out<sc_ufixed<16, 0, SC_RND, SC_SAT>> oX;
	sc_out<sc_ufixed<16, 0, SC_RND, SC_SAT>> oY;

	SC_CTOR(CordicCC)
	{
		SC_CTHREAD(doWork, clk.pos());
		reset_signal_is(nrst, false);
	}

private:
	void doWork();
};