#pragma once

unsigned int CordicRdCtl(unsigned int adr);

void CordicWrPhi(unsigned int adr, unsigned int data);

unsigned int CordicRdXY(unsigned int adr);

#ifdef EMUCPUINSTANCE
void WaitForIrq();
#endif