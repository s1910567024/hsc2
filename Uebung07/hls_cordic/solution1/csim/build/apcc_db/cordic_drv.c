/* Provide Declarations */
#include <stdarg.h>
#include <setjmp.h>
#include <limits.h>
#ifdef NEED_CBEAPINT
#include <autopilot_cbe.h>
#else
#define aesl_fopen fopen
#define aesl_freopen freopen
#define aesl_tmpfile tmpfile
#endif
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#ifdef __STRICT_ANSI__
#define inline __inline__
#define typeof __typeof__ 
#endif
#define __isoc99_fscanf fscanf
#define __isoc99_sscanf sscanf
#undef ferror
#undef feof
/* get a declaration for alloca */
#if defined(__CYGWIN__) || defined(__MINGW32__)
#define  alloca(x) __builtin_alloca((x))
#define _alloca(x) __builtin_alloca((x))
#elif defined(__APPLE__)
extern void *__builtin_alloca(unsigned long);
#define alloca(x) __builtin_alloca(x)
#define longjmp _longjmp
#define setjmp _setjmp
#elif defined(__sun__)
#if defined(__sparcv9)
extern void *__builtin_alloca(unsigned long);
#else
extern void *__builtin_alloca(unsigned int);
#endif
#define alloca(x) __builtin_alloca(x)
#elif defined(__FreeBSD__) || defined(__NetBSD__) || defined(__OpenBSD__) || defined(__DragonFly__) || defined(__arm__)
#define alloca(x) __builtin_alloca(x)
#elif defined(_MSC_VER)
#define inline _inline
#define alloca(x) _alloca(x)
#else
#include <alloca.h>
#endif

#ifndef __GNUC__  /* Can only support "linkonce" vars with GCC */
#define __attribute__(X)
#endif

#if defined(__GNUC__) && defined(__APPLE_CC__)
#define __EXTERNAL_WEAK__ __attribute__((weak_import))
#elif defined(__GNUC__)
#define __EXTERNAL_WEAK__ __attribute__((weak))
#else
#define __EXTERNAL_WEAK__
#endif

#if defined(__GNUC__) && (defined(__APPLE_CC__) || defined(__CYGWIN__) || defined(__MINGW32__))
#define __ATTRIBUTE_WEAK__
#elif defined(__GNUC__)
#define __ATTRIBUTE_WEAK__ __attribute__((weak))
#else
#define __ATTRIBUTE_WEAK__
#endif

#if defined(__GNUC__)
#define __HIDDEN__ __attribute__((visibility("hidden")))
#endif

#ifdef __GNUC__
#define LLVM_NAN(NanStr)   __builtin_nan(NanStr)   /* Double */
#define LLVM_NANF(NanStr)  __builtin_nanf(NanStr)  /* Float */
#define LLVM_NANS(NanStr)  __builtin_nans(NanStr)  /* Double */
#define LLVM_NANSF(NanStr) __builtin_nansf(NanStr) /* Float */
#define LLVM_INF           __builtin_inf()         /* Double */
#define LLVM_INFF          __builtin_inff()        /* Float */
#define LLVM_PREFETCH(addr,rw,locality) __builtin_prefetch(addr,rw,locality)
#define __ATTRIBUTE_CTOR__ __attribute__((constructor))
#define __ATTRIBUTE_DTOR__ __attribute__((destructor))
#define LLVM_ASM           __asm__
#else
#define LLVM_NAN(NanStr)   ((double)0.0)           /* Double */
#define LLVM_NANF(NanStr)  0.0F                    /* Float */
#define LLVM_NANS(NanStr)  ((double)0.0)           /* Double */
#define LLVM_NANSF(NanStr) 0.0F                    /* Float */
#define LLVM_INF           ((double)0.0)           /* Double */
#define LLVM_INFF          0.0F                    /* Float */
#define LLVM_PREFETCH(addr,rw,locality)            /* PREFETCH */
#define __ATTRIBUTE_CTOR__
#define __ATTRIBUTE_DTOR__
#define LLVM_ASM(X)
#endif

#if __GNUC__ < 4 /* Old GCC's, or compilers not GCC */ 
#define __builtin_stack_save() 0   /* not implemented */
#define __builtin_stack_restore(X) /* noop */
#endif

#if __GNUC__ && __LP64__ /* 128-bit integer types */
typedef int __attribute__((mode(TI))) llvmInt128;
typedef unsigned __attribute__((mode(TI))) llvmUInt128;
#endif

#define CODE_FOR_MAIN() /* Any target-specific code for main()*/

#ifndef __cplusplus
typedef unsigned char bool;
#endif


/* Support for floating point constants */
typedef unsigned long long ConstantDoubleTy;
typedef unsigned int        ConstantFloatTy;
typedef struct { unsigned long long f1; unsigned short f2; unsigned short pad[3]; } ConstantFP80Ty;
typedef struct { unsigned long long f1; unsigned long long f2; } ConstantFP128Ty;


/* Global Declarations */
/* Helper union for bitcasts */
typedef union {
  unsigned int Int32;
  unsigned long long Int64;
  float Float;
  double Double;
} llvmBitCastUnion;
/* Structure forward decls */
typedef struct l_struct_OC_XCordic l_struct_OC_XCordic;
typedef struct l_struct_OC_XScuGic l_struct_OC_XScuGic;
typedef struct l_struct_OC_XScuGic_Config l_struct_OC_XScuGic_Config;
typedef struct l_struct_OC_XScuGic_VectorTableEntry l_struct_OC_XScuGic_VectorTableEntry;

/* Structure contents */
struct l_struct_OC_XCordic {
  signed int *field0;
  unsigned int field1;
};

struct l_struct_OC_XScuGic {
  l_struct_OC_XScuGic_Config *field0;
  unsigned int field1;
  unsigned int field2;
};

struct l_struct_OC_XScuGic_VectorTableEntry {
  void  (*field0) ( char *);
   char *field1;
};

struct l_struct_OC_XScuGic_Config {
  unsigned short field0;
  unsigned int field1;
  unsigned int field2;
  l_struct_OC_XScuGic_VectorTableEntry field3[95];
};


/* External Global Variable Declarations */

/* Function Declarations */
double fmod(double, double);
float fmodf(float, float);
long double fmodl(long double, long double);
void Xil_ExceptionEnable(void);
void XScuGic_Enable(l_struct_OC_XScuGic *llvm_cbe_InstancePtr, signed int llvm_cbe_Int_Id);
signed int XScuGic_Connect(l_struct_OC_XScuGic *llvm_cbe_InstancePtr, signed int llvm_cbe_Int_Id, void  (*llvm_cbe_Handler) ( char *),  char *llvm_cbe_CallBackRef);
void Xil_ExceptionRegisterHandler(signed int llvm_cbe_Exception_id, void  (*llvm_cbe_Handler) ( char *),  char *llvm_cbe_Data);
void Xil_ExceptionInit(void);
signed int XScuGic_CfgInitialize(l_struct_OC_XScuGic *llvm_cbe_InstancePtr, l_struct_OC_XScuGic_Config *llvm_cbe_ConfigPtr, signed int llvm_cbe_EffectiveAddr);
l_struct_OC_XScuGic_Config *XScuGic_LookupConfig(signed short llvm_cbe_DeviceId);
void XScuGic_InterruptHandler(l_struct_OC_XScuGic *llvm_cbe_InstancePtr);
bool CordicCalcXY(float *llvm_cbe_phi, float *llvm_cbe_cos, float *llvm_cbe_sin, signed int *llvm_cbe_adr);
void CordicWrPhi(signed int , signed int );
void WaitForIrq();
signed int CordicRdXY(signed int );
bool cordic_init(void);
signed int XCordic_Initialize(l_struct_OC_XCordic *, signed short );


/* Global Variable Definitions and Initialization */
static bool aesl_internal_rdy_irq;
static l_struct_OC_XCordic aesl_internal_CordicInstance;


/* Function Bodies */
static inline int llvm_fcmp_ord(double X, double Y) { return X == X && Y == Y; }
static inline int llvm_fcmp_uno(double X, double Y) { return X != X || Y != Y; }
static inline int llvm_fcmp_ueq(double X, double Y) { return X == Y || llvm_fcmp_uno(X, Y); }
static inline int llvm_fcmp_une(double X, double Y) { return X != Y; }
static inline int llvm_fcmp_ult(double X, double Y) { return X <  Y || llvm_fcmp_uno(X, Y); }
static inline int llvm_fcmp_ugt(double X, double Y) { return X >  Y || llvm_fcmp_uno(X, Y); }
static inline int llvm_fcmp_ule(double X, double Y) { return X <= Y || llvm_fcmp_uno(X, Y); }
static inline int llvm_fcmp_uge(double X, double Y) { return X >= Y || llvm_fcmp_uno(X, Y); }
static inline int llvm_fcmp_oeq(double X, double Y) { return X == Y ; }
static inline int llvm_fcmp_one(double X, double Y) { return X != Y && llvm_fcmp_ord(X, Y); }
static inline int llvm_fcmp_olt(double X, double Y) { return X <  Y ; }
static inline int llvm_fcmp_ogt(double X, double Y) { return X >  Y ; }
static inline int llvm_fcmp_ole(double X, double Y) { return X <= Y ; }
static inline int llvm_fcmp_oge(double X, double Y) { return X >= Y ; }

void Xil_ExceptionEnable(void) {
  static  unsigned long long aesl_llvm_cbe_1_count = 0;
const char* AESL_DEBUG_TRACE = getenv("DEBUG_TRACE");
if (AESL_DEBUG_TRACE)
printf("\n\{ BEGIN @Xil_ExceptionEnable\n");
  if (AESL_DEBUG_TRACE)
      printf("\nEND @Xil_ExceptionEnable}\n");
  return;
}


void XScuGic_Enable(l_struct_OC_XScuGic *llvm_cbe_InstancePtr, signed int llvm_cbe_Int_Id) {
  static  unsigned long long aesl_llvm_cbe_2_count = 0;
  static  unsigned long long aesl_llvm_cbe_3_count = 0;
  static  unsigned long long aesl_llvm_cbe_4_count = 0;
const char* AESL_DEBUG_TRACE = getenv("DEBUG_TRACE");
if (AESL_DEBUG_TRACE)
printf("\n\{ BEGIN @XScuGic_Enable\n");
  if (AESL_DEBUG_TRACE)
      printf("\nEND @XScuGic_Enable}\n");
  return;
}


signed int XScuGic_Connect(l_struct_OC_XScuGic *llvm_cbe_InstancePtr, signed int llvm_cbe_Int_Id, void  (*llvm_cbe_Handler) ( char *),  char *llvm_cbe_CallBackRef) {
  static  unsigned long long aesl_llvm_cbe_5_count = 0;
  static  unsigned long long aesl_llvm_cbe_6_count = 0;
  static  unsigned long long aesl_llvm_cbe_7_count = 0;
  static  unsigned long long aesl_llvm_cbe_8_count = 0;
  static  unsigned long long aesl_llvm_cbe_9_count = 0;
const char* AESL_DEBUG_TRACE = getenv("DEBUG_TRACE");
if (AESL_DEBUG_TRACE)
printf("\n\{ BEGIN @XScuGic_Connect\n");
  if (AESL_DEBUG_TRACE)
      printf("\nEND @XScuGic_Connect}\n");
  return 0u;
}


void Xil_ExceptionRegisterHandler(signed int llvm_cbe_Exception_id, void  (*llvm_cbe_Handler) ( char *),  char *llvm_cbe_Data) {
  static  unsigned long long aesl_llvm_cbe_10_count = 0;
  static  unsigned long long aesl_llvm_cbe_11_count = 0;
  static  unsigned long long aesl_llvm_cbe_12_count = 0;
  static  unsigned long long aesl_llvm_cbe_13_count = 0;
const char* AESL_DEBUG_TRACE = getenv("DEBUG_TRACE");
if (AESL_DEBUG_TRACE)
printf("\n\{ BEGIN @Xil_ExceptionRegisterHandler\n");
  if (AESL_DEBUG_TRACE)
      printf("\nEND @Xil_ExceptionRegisterHandler}\n");
  return;
}


void Xil_ExceptionInit(void) {
  static  unsigned long long aesl_llvm_cbe_14_count = 0;
const char* AESL_DEBUG_TRACE = getenv("DEBUG_TRACE");
if (AESL_DEBUG_TRACE)
printf("\n\{ BEGIN @Xil_ExceptionInit\n");
  if (AESL_DEBUG_TRACE)
      printf("\nEND @Xil_ExceptionInit}\n");
  return;
}


signed int XScuGic_CfgInitialize(l_struct_OC_XScuGic *llvm_cbe_InstancePtr, l_struct_OC_XScuGic_Config *llvm_cbe_ConfigPtr, signed int llvm_cbe_EffectiveAddr) {
  static  unsigned long long aesl_llvm_cbe_15_count = 0;
  static  unsigned long long aesl_llvm_cbe_16_count = 0;
  static  unsigned long long aesl_llvm_cbe_17_count = 0;
  static  unsigned long long aesl_llvm_cbe_18_count = 0;
const char* AESL_DEBUG_TRACE = getenv("DEBUG_TRACE");
if (AESL_DEBUG_TRACE)
printf("\n\{ BEGIN @XScuGic_CfgInitialize\n");
  if (AESL_DEBUG_TRACE)
      printf("\nEND @XScuGic_CfgInitialize}\n");
  return 0u;
}


l_struct_OC_XScuGic_Config *XScuGic_LookupConfig(signed short llvm_cbe_DeviceId) {
  static  unsigned long long aesl_llvm_cbe_19_count = 0;
  static  unsigned long long aesl_llvm_cbe_20_count = 0;
const char* AESL_DEBUG_TRACE = getenv("DEBUG_TRACE");
if (AESL_DEBUG_TRACE)
printf("\n\{ BEGIN @XScuGic_LookupConfig\n");
  if (AESL_DEBUG_TRACE)
      printf("\nEND @XScuGic_LookupConfig}\n");
  return ((l_struct_OC_XScuGic_Config *)/*NULL*/0);
}


void XScuGic_InterruptHandler(l_struct_OC_XScuGic *llvm_cbe_InstancePtr) {
  static  unsigned long long aesl_llvm_cbe_21_count = 0;
  static  unsigned long long aesl_llvm_cbe_22_count = 0;
const char* AESL_DEBUG_TRACE = getenv("DEBUG_TRACE");
if (AESL_DEBUG_TRACE)
printf("\n\{ BEGIN @XScuGic_InterruptHandler\n");
  if (AESL_DEBUG_TRACE)
      printf("\nEND @XScuGic_InterruptHandler}\n");
  return;
}


bool CordicCalcXY(float *llvm_cbe_phi, float *llvm_cbe_cos, float *llvm_cbe_sin, signed int *llvm_cbe_adr) {
  static  unsigned long long aesl_llvm_cbe_23_count = 0;
  static  unsigned long long aesl_llvm_cbe_24_count = 0;
  static  unsigned long long aesl_llvm_cbe_25_count = 0;
  static  unsigned long long aesl_llvm_cbe_26_count = 0;
  static  unsigned long long aesl_llvm_cbe_27_count = 0;
  static  unsigned long long aesl_llvm_cbe_28_count = 0;
  static  unsigned long long aesl_llvm_cbe_29_count = 0;
  static  unsigned long long aesl_llvm_cbe_30_count = 0;
  static  unsigned long long aesl_llvm_cbe_31_count = 0;
  static  unsigned long long aesl_llvm_cbe_32_count = 0;
  static  unsigned long long aesl_llvm_cbe_33_count = 0;
  static  unsigned long long aesl_llvm_cbe_34_count = 0;
  static  unsigned long long aesl_llvm_cbe_35_count = 0;
  static  unsigned long long aesl_llvm_cbe_36_count = 0;
  static  unsigned long long aesl_llvm_cbe_37_count = 0;
  static  unsigned long long aesl_llvm_cbe_38_count = 0;
  static  unsigned long long aesl_llvm_cbe_39_count = 0;
  static  unsigned long long aesl_llvm_cbe_40_count = 0;
  static  unsigned long long aesl_llvm_cbe_41_count = 0;
  static  unsigned long long aesl_llvm_cbe_42_count = 0;
  static  unsigned long long aesl_llvm_cbe_43_count = 0;
  static  unsigned long long aesl_llvm_cbe_44_count = 0;
  float llvm_cbe_tmp__1;
  static  unsigned long long aesl_llvm_cbe_45_count = 0;
  double llvm_cbe_tmp__2;
  static  unsigned long long aesl_llvm_cbe_46_count = 0;
  double llvm_cbe_tmp__3;
  static  unsigned long long aesl_llvm_cbe_47_count = 0;
  float llvm_cbe_tmp__4;
  static  unsigned long long aesl_llvm_cbe_48_count = 0;
  static  unsigned long long aesl_llvm_cbe_49_count = 0;
  static  unsigned long long aesl_llvm_cbe_50_count = 0;
  static  unsigned long long aesl_llvm_cbe_51_count = 0;
  static  unsigned long long aesl_llvm_cbe_52_count = 0;
  static  unsigned long long aesl_llvm_cbe_53_count = 0;
  static  unsigned long long aesl_llvm_cbe_54_count = 0;
  static  unsigned long long aesl_llvm_cbe_55_count = 0;
  static  unsigned long long aesl_llvm_cbe_56_count = 0;
  static  unsigned long long aesl_llvm_cbe_57_count = 0;
  double llvm_cbe_tmp__5;
  static  unsigned long long aesl_llvm_cbe_58_count = 0;
  double llvm_cbe_tmp__6;
  static  unsigned long long aesl_llvm_cbe_59_count = 0;
  float llvm_cbe_tmp__7;
  static  unsigned long long aesl_llvm_cbe_60_count = 0;
  static  unsigned long long aesl_llvm_cbe_61_count = 0;
  static  unsigned long long aesl_llvm_cbe_62_count = 0;
  static  unsigned long long aesl_llvm_cbe_63_count = 0;
  static  unsigned long long aesl_llvm_cbe_64_count = 0;
  static  unsigned long long aesl_llvm_cbe_65_count = 0;
  static  unsigned long long aesl_llvm_cbe_66_count = 0;
  static  unsigned long long aesl_llvm_cbe_67_count = 0;
  static  unsigned long long aesl_llvm_cbe_68_count = 0;
  float llvm_cbe_tmp__8;
  float llvm_cbe_tmp__8__PHI_TEMPORARY;
  static  unsigned long long aesl_llvm_cbe_69_count = 0;
  double llvm_cbe_tmp__9;
  static  unsigned long long aesl_llvm_cbe_70_count = 0;
  double llvm_cbe_tmp__10;
  static  unsigned long long aesl_llvm_cbe_71_count = 0;
  double llvm_cbe_tmp__11;
  static  unsigned long long aesl_llvm_cbe_72_count = 0;
  unsigned int llvm_cbe_tmp__12;
  static  unsigned long long aesl_llvm_cbe_73_count = 0;
  static  unsigned long long aesl_llvm_cbe_74_count = 0;
  static  unsigned long long aesl_llvm_cbe_75_count = 0;
  static  unsigned long long aesl_llvm_cbe_76_count = 0;
  static  unsigned long long aesl_llvm_cbe_77_count = 0;
  static  unsigned long long aesl_llvm_cbe_78_count = 0;
  unsigned int llvm_cbe_tmp__13;
  static  unsigned long long aesl_llvm_cbe_79_count = 0;
  static  unsigned long long aesl_llvm_cbe__2e_pr_count = 0;
  bool llvm_cbe__2e_pr;
  static  unsigned long long aesl_llvm_cbe_80_count = 0;
  static  unsigned long long aesl_llvm_cbe_81_count = 0;
  static  unsigned long long aesl_llvm_cbe_82_count = 0;
  static  unsigned long long aesl_llvm_cbe_83_count = 0;
  static  unsigned long long aesl_llvm_cbe_84_count = 0;
  unsigned int llvm_cbe_tmp__14;
  static  unsigned long long aesl_llvm_cbe_85_count = 0;
  unsigned int llvm_cbe_tmp__15;
  static  unsigned long long aesl_llvm_cbe_86_count = 0;
  static  unsigned long long aesl_llvm_cbe_87_count = 0;
  static  unsigned long long aesl_llvm_cbe_88_count = 0;
  static  unsigned long long aesl_llvm_cbe_89_count = 0;
  static  unsigned long long aesl_llvm_cbe_90_count = 0;
  unsigned int llvm_cbe_tmp__16;
  static  unsigned long long aesl_llvm_cbe_91_count = 0;
  double llvm_cbe_tmp__17;
  static  unsigned long long aesl_llvm_cbe_92_count = 0;
  double llvm_cbe_tmp__18;
  static  unsigned long long aesl_llvm_cbe_93_count = 0;
  float llvm_cbe_tmp__19;
  static  unsigned long long aesl_llvm_cbe_94_count = 0;
  static  unsigned long long aesl_llvm_cbe_95_count = 0;
  unsigned int llvm_cbe_tmp__20;
  static  unsigned long long aesl_llvm_cbe_96_count = 0;
  double llvm_cbe_tmp__21;
  static  unsigned long long aesl_llvm_cbe_97_count = 0;
  double llvm_cbe_tmp__22;
  static  unsigned long long aesl_llvm_cbe_98_count = 0;
  float llvm_cbe_tmp__23;
  static  unsigned long long aesl_llvm_cbe_99_count = 0;
  static  unsigned long long aesl_llvm_cbe_100_count = 0;
  static  unsigned long long aesl_llvm_cbe_101_count = 0;
  static  unsigned long long aesl_llvm_cbe_102_count = 0;
  static  unsigned long long aesl_llvm_cbe_103_count = 0;
  static  unsigned long long aesl_llvm_cbe_104_count = 0;
  float llvm_cbe_tmp__24;
  static  unsigned long long aesl_llvm_cbe_105_count = 0;
  static  unsigned long long aesl_llvm_cbe_106_count = 0;
  static  unsigned long long aesl_llvm_cbe_107_count = 0;
  float llvm_cbe_tmp__25;
  static  unsigned long long aesl_llvm_cbe_108_count = 0;
  static  unsigned long long aesl_llvm_cbe_109_count = 0;
  static  unsigned long long aesl_llvm_cbe_110_count = 0;
  static  unsigned long long aesl_llvm_cbe_111_count = 0;
  static  unsigned long long aesl_llvm_cbe_112_count = 0;
  static  unsigned long long aesl_llvm_cbe_113_count = 0;
  float llvm_cbe_tmp__26;
  static  unsigned long long aesl_llvm_cbe_114_count = 0;
  float llvm_cbe_tmp__27;
  static  unsigned long long aesl_llvm_cbe_115_count = 0;
  static  unsigned long long aesl_llvm_cbe_116_count = 0;
  float llvm_cbe_tmp__28;
  static  unsigned long long aesl_llvm_cbe_117_count = 0;
  float llvm_cbe_tmp__29;
  static  unsigned long long aesl_llvm_cbe_118_count = 0;
  static  unsigned long long aesl_llvm_cbe_119_count = 0;
  static  unsigned long long aesl_llvm_cbe_120_count = 0;
  static  unsigned long long aesl_llvm_cbe_121_count = 0;
  static  unsigned long long aesl_llvm_cbe_122_count = 0;
  float llvm_cbe_tmp__30;
  static  unsigned long long aesl_llvm_cbe_123_count = 0;
  float llvm_cbe_tmp__31;
  static  unsigned long long aesl_llvm_cbe_124_count = 0;
  static  unsigned long long aesl_llvm_cbe_125_count = 0;
  static  unsigned long long aesl_llvm_cbe_126_count = 0;
  static  unsigned long long aesl_llvm_cbe_127_count = 0;
  static  unsigned long long aesl_llvm_cbe_128_count = 0;
  static  unsigned long long aesl_llvm_cbe_129_count = 0;

const char* AESL_DEBUG_TRACE = getenv("DEBUG_TRACE");
if (AESL_DEBUG_TRACE)
printf("\n\{ BEGIN @CordicCalcXY\n");
if (AESL_DEBUG_TRACE)
printf("\n  %%1 = load float* %%phi, align 4, !dbg !17 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_44_count);
  llvm_cbe_tmp__1 = (float )*llvm_cbe_phi;
if (AESL_DEBUG_TRACE)
printf("\n = %f,  0x%x\n", llvm_cbe_tmp__1, *(int*)(&llvm_cbe_tmp__1));
if (AESL_DEBUG_TRACE)
printf("\n  %%2 = fpext float %%1 to double, !dbg !17 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_45_count);
  llvm_cbe_tmp__2 = (double )((double )llvm_cbe_tmp__1);
if (AESL_DEBUG_TRACE)
printf("\n = %lf,  0x%llx\n", llvm_cbe_tmp__2, *(long long*)(&llvm_cbe_tmp__2));
if (AESL_DEBUG_TRACE)
printf("\n  %%3 = tail call double @fmod(double %%2, double 0x401921FB54442D18) nounwind, !dbg !17 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_46_count);
  llvm_cbe_tmp__3 = (double ) /*tail*/ fmod(llvm_cbe_tmp__2, 0x1.921fb54442d18p2);
if (AESL_DEBUG_TRACE) {
printf("\nArgument  = %lf,  0x%llx",llvm_cbe_tmp__2, *(long long*)(&llvm_cbe_tmp__2));
printf("\nArgument  = %lf",0x1.921fb54442d18p2);
printf("\nReturn  = %lf",llvm_cbe_tmp__3);
}
if (AESL_DEBUG_TRACE)
printf("\n  %%4 = fptrunc double %%3 to float, !dbg !17 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_47_count);
  llvm_cbe_tmp__4 = (float )((float )llvm_cbe_tmp__3);
if (AESL_DEBUG_TRACE)
printf("\n = %f,  0x%x\n", llvm_cbe_tmp__4, *(int*)(&llvm_cbe_tmp__4));
  if ((llvm_fcmp_olt(llvm_cbe_tmp__4, 0x0p0))) {
    goto llvm_cbe_tmp__32;
  } else {
    llvm_cbe_tmp__8__PHI_TEMPORARY = (float )llvm_cbe_tmp__4;   /* for PHI node */
    goto llvm_cbe_tmp__33;
  }

llvm_cbe_tmp__32:
if (AESL_DEBUG_TRACE)
printf("\n  %%7 = fpext float %%4 to double, !dbg !17 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_57_count);
  llvm_cbe_tmp__5 = (double )((double )llvm_cbe_tmp__4);
if (AESL_DEBUG_TRACE)
printf("\n = %lf,  0x%llx\n", llvm_cbe_tmp__5, *(long long*)(&llvm_cbe_tmp__5));
if (AESL_DEBUG_TRACE)
printf("\n  %%8 = fadd double %%7, 0x401921FB54442D18, !dbg !17 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_58_count);
  llvm_cbe_tmp__6 = (double )llvm_cbe_tmp__5 + 0x1.921fb54442d18p2;
if (AESL_DEBUG_TRACE)
printf("\n = %lf,  0x%llx\n", llvm_cbe_tmp__6, *(long long*)(&llvm_cbe_tmp__6));
if (AESL_DEBUG_TRACE)
printf("\n  %%9 = fptrunc double %%8 to float, !dbg !17 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_59_count);
  llvm_cbe_tmp__7 = (float )((float )llvm_cbe_tmp__6);
if (AESL_DEBUG_TRACE)
printf("\n = %f,  0x%x\n", llvm_cbe_tmp__7, *(int*)(&llvm_cbe_tmp__7));
  llvm_cbe_tmp__8__PHI_TEMPORARY = (float )llvm_cbe_tmp__7;   /* for PHI node */
  goto llvm_cbe_tmp__33;

llvm_cbe_tmp__33:
if (AESL_DEBUG_TRACE)
printf("\n  %%11 = phi float [ %%9, %%6 ], [ %%4, %%0  for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_68_count);
  llvm_cbe_tmp__8 = (float )llvm_cbe_tmp__8__PHI_TEMPORARY;
if (AESL_DEBUG_TRACE) {
printf("\n = %f",llvm_cbe_tmp__8);
printf("\n = %f",llvm_cbe_tmp__7);
printf("\n = %f",llvm_cbe_tmp__4);
}
if (AESL_DEBUG_TRACE)
printf("\n  %%12 = fpext float %%11 to double, !dbg !17 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_69_count);
  llvm_cbe_tmp__9 = (double )((double )llvm_cbe_tmp__8);
if (AESL_DEBUG_TRACE)
printf("\n = %lf,  0x%llx\n", llvm_cbe_tmp__9, *(long long*)(&llvm_cbe_tmp__9));
if (AESL_DEBUG_TRACE)
printf("\n  %%13 = tail call double @fmod(double %%12, double 0x3FF921FB54442D18) nounwind, !dbg !17 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_70_count);
  llvm_cbe_tmp__10 = (double ) /*tail*/ fmod(llvm_cbe_tmp__9, 0x1.921fb54442d18p0);
if (AESL_DEBUG_TRACE) {
printf("\nArgument  = %lf,  0x%llx",llvm_cbe_tmp__9, *(long long*)(&llvm_cbe_tmp__9));
printf("\nArgument  = %lf",0x1.921fb54442d18p0);
printf("\nReturn  = %lf",llvm_cbe_tmp__10);
}
if (AESL_DEBUG_TRACE)
printf("\n  %%14 = fmul double %%13, 2.097152e+06, !dbg !17 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_71_count);
  llvm_cbe_tmp__11 = (double )llvm_cbe_tmp__10 * 0x1p21;
if (AESL_DEBUG_TRACE)
printf("\n = %lf,  0x%llx\n", llvm_cbe_tmp__11, *(long long*)(&llvm_cbe_tmp__11));
if (AESL_DEBUG_TRACE)
printf("\n  %%15 = fptoui double %%14 to i32, !dbg !17 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_72_count);
  llvm_cbe_tmp__12 = (unsigned int )((unsigned int )llvm_cbe_tmp__11&4294967295U);
if (AESL_DEBUG_TRACE)
printf("\n = 0x%X\n", llvm_cbe_tmp__12);
if (AESL_DEBUG_TRACE)
printf("\n  store i1 false, i1* @aesl_internal_rdy_irq, align 1, !dbg !18 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_77_count);
  *(&aesl_internal_rdy_irq) = ((0) & 1ull);
if (AESL_DEBUG_TRACE)
printf("\n = 0x%X\n", 0);
if (AESL_DEBUG_TRACE)
printf("\n  %%16 = load i32* %%adr, align 4, !dbg !16 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_78_count);
  llvm_cbe_tmp__13 = (unsigned int )*llvm_cbe_adr;
if (AESL_DEBUG_TRACE)
printf("\n = 0x%X\n", llvm_cbe_tmp__13);
if (AESL_DEBUG_TRACE)
printf("\n  tail call void @CordicWrPhi(i32 %%16, i32 %%15) nounwind, !dbg !16 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_79_count);
   /*tail*/ CordicWrPhi(llvm_cbe_tmp__13, llvm_cbe_tmp__12);
if (AESL_DEBUG_TRACE) {
printf("\nArgument  = 0x%X",llvm_cbe_tmp__13);
printf("\nArgument  = 0x%X",llvm_cbe_tmp__12);
}
if (AESL_DEBUG_TRACE)
printf("\n  %%.pr = load i1* @aesl_internal_rdy_irq, align 1, !dbg !18 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe__2e_pr_count);
  llvm_cbe__2e_pr = (bool )((*(&aesl_internal_rdy_irq))&1);
if (AESL_DEBUG_TRACE)
printf("\n.pr = 0x%X\n", llvm_cbe__2e_pr);
  if (llvm_cbe__2e_pr) {
    goto llvm_cbe_tmp__34;
  } else {
    goto llvm_cbe__2e_critedge;
  }

llvm_cbe__2e_critedge:
if (AESL_DEBUG_TRACE)
printf("\n  tail call void bitcast (void (...)* @WaitForIrq to void ()*)() nounwind, !dbg !18 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_81_count);
   /*tail*/ WaitForIrq();
if (AESL_DEBUG_TRACE) {
}
if (AESL_DEBUG_TRACE)
printf("\n  store i1 true, i1* @aesl_internal_rdy_irq, align 1, !dbg !18 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_82_count);
  *(&aesl_internal_rdy_irq) = ((1) & 1ull);
if (AESL_DEBUG_TRACE)
printf("\n = 0x%X\n", 1);
  goto llvm_cbe_tmp__34;

llvm_cbe_tmp__34:
if (AESL_DEBUG_TRACE)
printf("\n  %%18 = load i32* %%adr, align 4, !dbg !16 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_84_count);
  llvm_cbe_tmp__14 = (unsigned int )*llvm_cbe_adr;
if (AESL_DEBUG_TRACE)
printf("\n = 0x%X\n", llvm_cbe_tmp__14);
if (AESL_DEBUG_TRACE)
printf("\n  %%19 = tail call i32 @CordicRdXY(i32 %%18) nounwind, !dbg !16 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_85_count);
  llvm_cbe_tmp__15 = (unsigned int ) /*tail*/ CordicRdXY(llvm_cbe_tmp__14);
if (AESL_DEBUG_TRACE) {
printf("\nArgument  = 0x%X",llvm_cbe_tmp__14);
printf("\nReturn  = 0x%X",llvm_cbe_tmp__15);
}
if (AESL_DEBUG_TRACE)
printf("\n  %%20 = lshr i32 %%19, 16, !dbg !15 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_90_count);
  llvm_cbe_tmp__16 = (unsigned int )((unsigned int )(((unsigned int )(llvm_cbe_tmp__15&4294967295ull)) >> ((unsigned int )(16u&4294967295ull))));
if (AESL_DEBUG_TRACE)
printf("\n = 0x%X\n", ((unsigned int )(llvm_cbe_tmp__16&4294967295ull)));
if (AESL_DEBUG_TRACE)
printf("\n  %%21 = uitofp i32 %%20 to double, !dbg !15 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_91_count);
  llvm_cbe_tmp__17 = (double )((double )(unsigned int )llvm_cbe_tmp__16);
if (AESL_DEBUG_TRACE)
printf("\n = %lf,  0x%llx\n", llvm_cbe_tmp__17, *(long long*)(&llvm_cbe_tmp__17));
if (AESL_DEBUG_TRACE)
printf("\n  %%22 = fmul double %%21, 0x3EF0000000000000, !dbg !15 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_92_count);
  llvm_cbe_tmp__18 = (double )llvm_cbe_tmp__17 * 0x1p-16;
if (AESL_DEBUG_TRACE)
printf("\n = %lf,  0x%llx\n", llvm_cbe_tmp__18, *(long long*)(&llvm_cbe_tmp__18));
if (AESL_DEBUG_TRACE)
printf("\n  %%23 = fptrunc double %%22 to float, !dbg !15 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_93_count);
  llvm_cbe_tmp__19 = (float )((float )llvm_cbe_tmp__18);
if (AESL_DEBUG_TRACE)
printf("\n = %f,  0x%x\n", llvm_cbe_tmp__19, *(int*)(&llvm_cbe_tmp__19));
if (AESL_DEBUG_TRACE)
printf("\n  store float %%23, float* %%cos, align 4, !dbg !15 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_94_count);
  *llvm_cbe_cos = llvm_cbe_tmp__19;
if (AESL_DEBUG_TRACE)
printf("\n = %f\n", llvm_cbe_tmp__19);
if (AESL_DEBUG_TRACE)
printf("\n  %%24 = and i32 %%19, 65535, !dbg !16 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_95_count);
  llvm_cbe_tmp__20 = (unsigned int )llvm_cbe_tmp__15 & 65535u;
if (AESL_DEBUG_TRACE)
printf("\n = 0x%X\n", llvm_cbe_tmp__20);
if (AESL_DEBUG_TRACE)
printf("\n  %%25 = uitofp i32 %%24 to double, !dbg !16 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_96_count);
  llvm_cbe_tmp__21 = (double )((double )(unsigned int )llvm_cbe_tmp__20);
if (AESL_DEBUG_TRACE)
printf("\n = %lf,  0x%llx\n", llvm_cbe_tmp__21, *(long long*)(&llvm_cbe_tmp__21));
if (AESL_DEBUG_TRACE)
printf("\n  %%26 = fmul double %%25, 0x3EF0000000000000, !dbg !16 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_97_count);
  llvm_cbe_tmp__22 = (double )llvm_cbe_tmp__21 * 0x1p-16;
if (AESL_DEBUG_TRACE)
printf("\n = %lf,  0x%llx\n", llvm_cbe_tmp__22, *(long long*)(&llvm_cbe_tmp__22));
if (AESL_DEBUG_TRACE)
printf("\n  %%27 = fptrunc double %%26 to float, !dbg !16 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_98_count);
  llvm_cbe_tmp__23 = (float )((float )llvm_cbe_tmp__22);
if (AESL_DEBUG_TRACE)
printf("\n = %f,  0x%x\n", llvm_cbe_tmp__23, *(int*)(&llvm_cbe_tmp__23));
if (AESL_DEBUG_TRACE)
printf("\n  store float %%27, float* %%sin, align 4, !dbg !16 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_99_count);
  *llvm_cbe_sin = llvm_cbe_tmp__23;
if (AESL_DEBUG_TRACE)
printf("\n = %f\n", llvm_cbe_tmp__23);
  if ((llvm_fcmp_olt(llvm_cbe_tmp__9, 0x1.921fb54442d18p0))) {
    goto llvm_cbe_tmp__35;
  } else {
    goto llvm_cbe_tmp__36;
  }

llvm_cbe_tmp__36:
  if ((llvm_fcmp_olt(llvm_cbe_tmp__9, 0x1.921fb54442d18p1))) {
    goto llvm_cbe_tmp__37;
  } else {
    goto llvm_cbe_tmp__38;
  }

llvm_cbe_tmp__37:
if (AESL_DEBUG_TRACE)
printf("\n  %%32 = load float* %%cos, align 4, !dbg !15 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_104_count);
  llvm_cbe_tmp__24 = (float )*llvm_cbe_cos;
if (AESL_DEBUG_TRACE)
printf("\n = %f,  0x%x\n", llvm_cbe_tmp__24, *(int*)(&llvm_cbe_tmp__24));
if (AESL_DEBUG_TRACE)
printf("\n  %%33 = fsub float -0.000000e+00, %%27, !dbg !15 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_107_count);
  llvm_cbe_tmp__25 = (float )((float )(-(llvm_cbe_tmp__23)));
if (AESL_DEBUG_TRACE)
printf("\n = %f,  0x%x\n", llvm_cbe_tmp__25, *(int*)(&llvm_cbe_tmp__25));
if (AESL_DEBUG_TRACE)
printf("\n  store float %%33, float* %%cos, align 4, !dbg !15 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_108_count);
  *llvm_cbe_cos = llvm_cbe_tmp__25;
if (AESL_DEBUG_TRACE)
printf("\n = %f\n", llvm_cbe_tmp__25);
if (AESL_DEBUG_TRACE)
printf("\n  store float %%32, float* %%sin, align 4, !dbg !16 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_109_count);
  *llvm_cbe_sin = llvm_cbe_tmp__24;
if (AESL_DEBUG_TRACE)
printf("\n = %f\n", llvm_cbe_tmp__24);
  goto llvm_cbe_tmp__39;

llvm_cbe_tmp__38:
  if ((llvm_fcmp_olt(llvm_cbe_tmp__9, 0x1.2d97c7f3321d2p2))) {
    goto llvm_cbe_tmp__40;
  } else {
    goto llvm_cbe_tmp__41;
  }

llvm_cbe_tmp__40:
if (AESL_DEBUG_TRACE)
printf("\n  %%37 = load float* %%cos, align 4, !dbg !15 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_113_count);
  llvm_cbe_tmp__26 = (float )*llvm_cbe_cos;
if (AESL_DEBUG_TRACE)
printf("\n = %f,  0x%x\n", llvm_cbe_tmp__26, *(int*)(&llvm_cbe_tmp__26));
if (AESL_DEBUG_TRACE)
printf("\n  %%38 = fsub float -0.000000e+00, %%37, !dbg !15 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_114_count);
  llvm_cbe_tmp__27 = (float )((float )(-(llvm_cbe_tmp__26)));
if (AESL_DEBUG_TRACE)
printf("\n = %f,  0x%x\n", llvm_cbe_tmp__27, *(int*)(&llvm_cbe_tmp__27));
if (AESL_DEBUG_TRACE)
printf("\n  store float %%38, float* %%cos, align 4, !dbg !15 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_115_count);
  *llvm_cbe_cos = llvm_cbe_tmp__27;
if (AESL_DEBUG_TRACE)
printf("\n = %f\n", llvm_cbe_tmp__27);
if (AESL_DEBUG_TRACE)
printf("\n  %%39 = load float* %%sin, align 4, !dbg !16 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_116_count);
  llvm_cbe_tmp__28 = (float )*llvm_cbe_sin;
if (AESL_DEBUG_TRACE)
printf("\n = %f,  0x%x\n", llvm_cbe_tmp__28, *(int*)(&llvm_cbe_tmp__28));
if (AESL_DEBUG_TRACE)
printf("\n  %%40 = fsub float -0.000000e+00, %%39, !dbg !16 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_117_count);
  llvm_cbe_tmp__29 = (float )((float )(-(llvm_cbe_tmp__28)));
if (AESL_DEBUG_TRACE)
printf("\n = %f,  0x%x\n", llvm_cbe_tmp__29, *(int*)(&llvm_cbe_tmp__29));
if (AESL_DEBUG_TRACE)
printf("\n  store float %%40, float* %%sin, align 4, !dbg !16 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_118_count);
  *llvm_cbe_sin = llvm_cbe_tmp__29;
if (AESL_DEBUG_TRACE)
printf("\n = %f\n", llvm_cbe_tmp__29);
  goto llvm_cbe_tmp__42;

llvm_cbe_tmp__41:
if (AESL_DEBUG_TRACE)
printf("\n  %%42 = load float* %%cos, align 4, !dbg !15 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_122_count);
  llvm_cbe_tmp__30 = (float )*llvm_cbe_cos;
if (AESL_DEBUG_TRACE)
printf("\n = %f,  0x%x\n", llvm_cbe_tmp__30, *(int*)(&llvm_cbe_tmp__30));
if (AESL_DEBUG_TRACE)
printf("\n  %%43 = fsub float -0.000000e+00, %%42, !dbg !15 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_123_count);
  llvm_cbe_tmp__31 = (float )((float )(-(llvm_cbe_tmp__30)));
if (AESL_DEBUG_TRACE)
printf("\n = %f,  0x%x\n", llvm_cbe_tmp__31, *(int*)(&llvm_cbe_tmp__31));
if (AESL_DEBUG_TRACE)
printf("\n  store float %%43, float* %%sin, align 4, !dbg !15 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_124_count);
  *llvm_cbe_sin = llvm_cbe_tmp__31;
if (AESL_DEBUG_TRACE)
printf("\n = %f\n", llvm_cbe_tmp__31);
if (AESL_DEBUG_TRACE)
printf("\n  store float %%27, float* %%cos, align 4, !dbg !15 for 0x%I64xth hint within @CordicCalcXY  --> \n", ++aesl_llvm_cbe_125_count);
  *llvm_cbe_cos = llvm_cbe_tmp__23;
if (AESL_DEBUG_TRACE)
printf("\n = %f\n", llvm_cbe_tmp__23);
  goto llvm_cbe_tmp__42;

llvm_cbe_tmp__42:
  goto llvm_cbe_tmp__39;

llvm_cbe_tmp__39:
  goto llvm_cbe_tmp__35;

llvm_cbe_tmp__35:
  if (AESL_DEBUG_TRACE)
      printf("\nEND @CordicCalcXY}\n");
  return 0;
}


bool cordic_init(void) {
  static  unsigned long long aesl_llvm_cbe_130_count = 0;
  unsigned int llvm_cbe_tmp__43;
  static  unsigned long long aesl_llvm_cbe_131_count = 0;
  static  unsigned long long aesl_llvm_cbe_132_count = 0;
  static  unsigned long long aesl_llvm_cbe_133_count = 0;

const char* AESL_DEBUG_TRACE = getenv("DEBUG_TRACE");
if (AESL_DEBUG_TRACE)
printf("\n\{ BEGIN @cordic_init\n");
if (AESL_DEBUG_TRACE)
printf("\n  %%1 = tail call i32 @XCordic_Initialize(%%struct.XCordic* @aesl_internal_CordicInstance, i16 signext 0) nounwind, !dbg !14 for 0x%I64xth hint within @cordic_init  --> \n", ++aesl_llvm_cbe_130_count);
   /*tail*/ XCordic_Initialize((l_struct_OC_XCordic *)(&aesl_internal_CordicInstance), ((unsigned short )0));
if (AESL_DEBUG_TRACE) {
printf("\nArgument  = 0x%X",((unsigned short )0));
printf("\nReturn  = 0x%X",llvm_cbe_tmp__43);
}
  if (AESL_DEBUG_TRACE)
      printf("\nEND @cordic_init}\n");
  return 1;
}

