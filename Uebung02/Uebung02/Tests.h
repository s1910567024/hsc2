#pragma once

#include <iostream>
#include <assert.h>
#include <iomanip>
#include "StopWatch.h"
#include <tlm.h>
#include <systemc.h>

template<typename T>
void doTests(T& Master);

#include "MasterLT.h"


static const int cMemSize = 256;
static const int cWordWidth = 4;

//Wait delay for CC
template<typename T>
void waitDelay(T& Master)
{}

//Wait delay for LT
template<>
void waitDelay<MasterLT>(MasterLT& Master)
{
	if(LT_ACCUMULATE_DELAYS)
	{
		Master.waitDelay();
	}
}

template<typename T>
void writeSequential(T& Master) 
{
	std::cout << "Test WriteSequential Started!" << std::endl;
	stw::Start();

	for (int i = 0; i < cMemSize; i++)
	{
		Master.singleWrite(i*cWordWidth, i);
	}

	waitDelay(Master);

	double elapsed = stw::Stop_ms();
	std::cout << "Test WriteSequential finished!" << std::endl;
	std::cout << "Execution Time[ms]:" << elapsed << std::endl;
	std::cout << endl;
}

template<typename T>
void readSequential(T& Master)
{

	std::cout << "Test ReadSequential started!" << std::endl;
	stw::Start();

	for (int i = 0; i < cMemSize; i++)
	{
		int data = Master.singleRead(i*cWordWidth);
		
		// check value
		assert(data == i);
		if (data != i)
		{
			std::cerr << "Inconsistent Value!!" << std::endl;
		}
	}

	waitDelay(Master);

	double elapsed = stw::Stop_ms();
	std::cout << "Test ReadSequential finished!" << std::endl;
	std::cout << "Execution Time[ms]:" << elapsed << std::endl;
	std::cout << endl;
}

template<typename T>
void readRandom(T& Master)
{
	const int cRandSize = 1000000;

	std::cout << "Test ReadRandom started!" << std::endl;
	stw::Start();

	for (int i = 0; i < cRandSize; i++)
	{
		int randAddress = rand() % (cMemSize*4);
		int randVal = rand() % 0xFFFFFFFF;

		Master.singleWrite(randAddress,randVal);
		int data = Master.singleRead(randAddress);

		// check value
		assert(data == randVal);
		if (data != randVal)
		{
			std::cerr << "Inconsistent Value!!" << std::endl;
		}
	}

	waitDelay(Master);

	double elapsed = stw::Stop_ms();
	std::cout << "Test ReadRandom finished!" << std::endl;
	std::cout << "Execution Time[ms]:" << elapsed << std::endl;
	std::cout << endl;

}



template<typename T>
void doTests(T & Master)
{
	// Testcase #1: Write sequential data from Master to Mem.
	writeSequential(Master);
	
	// Testcase #2: Read sequential data from Mem to Master.
	readSequential(Master);
	
	// Testcase #3: Read random data from Mem to Master.
	readRandom(Master);

	std::cout << "Simulation took " << sc_delta_count() << " delta cycles!" << std::endl;
	
	sc_stop();
}


