#ifndef SRC_I2C_H_
#define SRC_I2C_H_

u8 LIS2DS12_WriteReg(u8 Reg, u8 *Bufp, u16 len);

u8 LIS2DS12_ReadReg(u8 Reg, u8 *Bufp, u16 len);

void sensor_init(void);

void read_temperature(void);

int u16_2s_complement_to_int(u16 word_to_convert);

void read_motion(void);

/*int main(void)
{
	sensor_init();

	while (1)
	{
		read_temperature();
		read_motion();
		//printf("\r\n"); //Blank line
		sleep(1); //seconds
	}

	return 0;
} //main()*/

#endif /* SRC_I2C_H_ */
