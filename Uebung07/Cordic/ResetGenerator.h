#pragma once

#include "mySystemc.h"

SC_MODULE(ResetGenerator)
{
	sc_out<bool> nrst;
	
	static const int NSUntilReset{ 40 };

	SC_CTOR(ResetGenerator)
	{
		SC_THREAD(doWork);
	}

private:
	void doWork()
	{
		nrst = false;
		wait(sc_time(NSUntilReset, SC_NS));
		nrst = true;
	}
};