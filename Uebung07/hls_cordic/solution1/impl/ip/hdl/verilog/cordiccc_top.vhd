-- ==============================================================
-- Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- ==============================================================
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

entity cordiccc_top is
    generic (
        C_S_AXI_CORDIC_IF_ADDR_WIDTH :  integer := 6;
        C_S_AXI_CORDIC_IF_DATA_WIDTH :  integer := 32;
        RESET_ACTIVE_LOW :  integer := 1
    );
    port (
        s_axi_cordic_if_AWADDR : in  std_logic_vector(C_S_AXI_CORDIC_IF_ADDR_WIDTH - 1 downto 0);
        s_axi_cordic_if_AWVALID : in  std_logic;
        s_axi_cordic_if_AWREADY : out  std_logic;
        s_axi_cordic_if_WDATA : in  std_logic_vector(C_S_AXI_CORDIC_IF_DATA_WIDTH - 1 downto 0);
        s_axi_cordic_if_WSTRB : in  std_logic_vector(C_S_AXI_CORDIC_IF_DATA_WIDTH/8 - 1 downto 0);
        s_axi_cordic_if_WVALID : in  std_logic;
        s_axi_cordic_if_WREADY : out  std_logic;
        s_axi_cordic_if_BRESP : out  std_logic_vector(2 - 1 downto 0);
        s_axi_cordic_if_BVALID : out  std_logic;
        s_axi_cordic_if_BREADY : in  std_logic;
        s_axi_cordic_if_ARADDR : in  std_logic_vector(C_S_AXI_CORDIC_IF_ADDR_WIDTH - 1 downto 0);
        s_axi_cordic_if_ARVALID : in  std_logic;
        s_axi_cordic_if_ARREADY : out  std_logic;
        s_axi_cordic_if_RDATA : out  std_logic_vector(C_S_AXI_CORDIC_IF_DATA_WIDTH - 1 downto 0);
        s_axi_cordic_if_RRESP : out  std_logic_vector(2 - 1 downto 0);
        s_axi_cordic_if_RVALID : out  std_logic;
        s_axi_cordic_if_RREADY : in  std_logic;
        aresetn : in  std_logic;
        aclk : in  std_logic
    );

-- attributes begin
-- attributes end
end entity;

architecture behav of cordiccc_top is
    component CordicCC is
        port (
            iStart : in  std_logic_vector(1 - 1 downto 0);
            oRdy : out  std_logic_vector(1 - 1 downto 0);
            iPhi : in  std_logic_vector(22 - 1 downto 0);
            oX : out  std_logic_vector(16 - 1 downto 0);
            oY : out  std_logic_vector(16 - 1 downto 0);
            nrst : in  std_logic;
            clk : in  std_logic
        );
    end component;

    component CordicCC_cordic_if_if is
        generic (
            C_ADDR_WIDTH :  integer;
            C_DATA_WIDTH :  integer);
        port (
            ACLK :  in std_logic;
            ARESETN :  in std_logic;
            I_iStart :  out std_logic_vector(1 - 1 downto 0);
            O_oRdy :  in std_logic_vector(1 - 1 downto 0);
            I_iPhi :  out std_logic_vector(22 - 1 downto 0);
            O_oX :  in std_logic_vector(16 - 1 downto 0);
            O_oY :  in std_logic_vector(16 - 1 downto 0);
            AWADDR :  in std_logic_vector(C_S_AXI_CORDIC_IF_ADDR_WIDTH - 1 downto 0);
            AWVALID :  in std_logic;
            AWREADY :  out std_logic;
            WDATA :  in std_logic_vector(C_S_AXI_CORDIC_IF_DATA_WIDTH - 1 downto 0);
            WSTRB :  in std_logic_vector(C_S_AXI_CORDIC_IF_DATA_WIDTH/8 - 1 downto 0);
            WVALID :  in std_logic;
            WREADY :  out std_logic;
            BRESP :  out std_logic_vector(2 - 1 downto 0);
            BVALID :  out std_logic;
            BREADY :  in std_logic;
            ARADDR :  in std_logic_vector(C_S_AXI_CORDIC_IF_ADDR_WIDTH - 1 downto 0);
            ARVALID :  in std_logic;
            ARREADY :  out std_logic;
            RDATA :  out std_logic_vector(C_S_AXI_CORDIC_IF_DATA_WIDTH - 1 downto 0);
            RRESP :  out std_logic_vector(2 - 1 downto 0);
            RVALID :  out std_logic;
            RREADY :  in std_logic);
    end component;

    component CordicCC_nrst_if is
        generic (
            RESET_ACTIVE_LOW :  integer);
        port (
            dout :  out std_logic;
            din :  in std_logic);
    end component;

    signal sig_CordicCC_iStart :  std_logic_vector(1 - 1 downto 0);
    signal sig_CordicCC_oRdy :  std_logic_vector(1 - 1 downto 0);
    signal sig_CordicCC_iPhi :  std_logic_vector(22 - 1 downto 0);
    signal sig_CordicCC_oX :  std_logic_vector(16 - 1 downto 0);
    signal sig_CordicCC_oY :  std_logic_vector(16 - 1 downto 0);
    signal sig_CordicCC_nrst :  std_logic;

begin
    CordicCC_U  : component CordicCC
        port map (
            iStart => sig_CordicCC_iStart,
            oRdy => sig_CordicCC_oRdy,
            iPhi => sig_CordicCC_iPhi,
            oX => sig_CordicCC_oX,
            oY => sig_CordicCC_oY,
            nrst => sig_CordicCC_nrst,
            clk => aclk
        );

    CordicCC_cordic_if_if_U : component CordicCC_cordic_if_if
        generic map (
            C_ADDR_WIDTH => C_S_AXI_CORDIC_IF_ADDR_WIDTH,
            C_DATA_WIDTH => C_S_AXI_CORDIC_IF_DATA_WIDTH)
        port map (
            ACLK => aclk,
            ARESETN => aresetn,
            I_iStart => sig_CordicCC_iStart,
            O_oRdy => sig_CordicCC_oRdy,
            I_iPhi => sig_CordicCC_iPhi,
            O_oX => sig_CordicCC_oX,
            O_oY => sig_CordicCC_oY,
            AWADDR => s_axi_cordic_if_AWADDR,
            AWVALID => s_axi_cordic_if_AWVALID,
            AWREADY => s_axi_cordic_if_AWREADY,
            WDATA => s_axi_cordic_if_WDATA,
            WSTRB => s_axi_cordic_if_WSTRB,
            WVALID => s_axi_cordic_if_WVALID,
            WREADY => s_axi_cordic_if_WREADY,
            BRESP => s_axi_cordic_if_BRESP,
            BVALID => s_axi_cordic_if_BVALID,
            BREADY => s_axi_cordic_if_BREADY,
            ARADDR => s_axi_cordic_if_ARADDR,
            ARVALID => s_axi_cordic_if_ARVALID,
            ARREADY => s_axi_cordic_if_ARREADY,
            RDATA => s_axi_cordic_if_RDATA,
            RRESP => s_axi_cordic_if_RRESP,
            RVALID => s_axi_cordic_if_RVALID,
            RREADY => s_axi_cordic_if_RREADY);

    nrst_if_U : component CordicCC_nrst_if
        generic map (
            RESET_ACTIVE_LOW => RESET_ACTIVE_LOW)
        port map (
            dout => sig_CordicCC_nrst,
            din => aresetn);

end architecture;

