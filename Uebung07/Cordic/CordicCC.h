#pragma once

#include "mySystemc.h"

#ifdef EMUCPUINSTANCE
#define cordic_in sc_ufixed<22, 1>
#define cordic_out sc_ufixed<16, 0, SC_RND, SC_SAT>
#else
#define cordic_in float
#define cordic_out float
#endif

SC_MODULE(CordicCC)
{
	sc_in<bool> clk;
	sc_in<bool> nrst;
	sc_in<bool> iStart;
	sc_out<bool> oRdy;
	sc_in<cordic_in> iPhi;
	sc_out<cordic_out> oX;
	sc_out<cordic_out> oY;

	SC_CTOR(CordicCC)
	{
		SC_CTHREAD(doWork, clk.pos());
		reset_signal_is(nrst, false);
	}

private:
	void doWork();
};
