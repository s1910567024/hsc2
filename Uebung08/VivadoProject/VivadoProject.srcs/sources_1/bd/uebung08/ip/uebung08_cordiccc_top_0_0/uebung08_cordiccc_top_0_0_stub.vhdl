-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Tue Jul  7 09:43:25 2020
-- Host        : DESKTOP-5PTVA4F running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub
--               E:/2Semester/HSC2/hsc2/Uebung08/VivadoProject/VivadoProject.srcs/sources_1/bd/uebung08/ip/uebung08_cordiccc_top_0_0/uebung08_cordiccc_top_0_0_stub.vhdl
-- Design      : uebung08_cordiccc_top_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z007sclg225-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity uebung08_cordiccc_top_0_0 is
  Port ( 
    s_axi_cordic_if_AWADDR : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s_axi_cordic_if_AWVALID : in STD_LOGIC;
    s_axi_cordic_if_AWREADY : out STD_LOGIC;
    s_axi_cordic_if_WDATA : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_cordic_if_WSTRB : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_cordic_if_WVALID : in STD_LOGIC;
    s_axi_cordic_if_WREADY : out STD_LOGIC;
    s_axi_cordic_if_BRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_cordic_if_BVALID : out STD_LOGIC;
    s_axi_cordic_if_BREADY : in STD_LOGIC;
    s_axi_cordic_if_ARADDR : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s_axi_cordic_if_ARVALID : in STD_LOGIC;
    s_axi_cordic_if_ARREADY : out STD_LOGIC;
    s_axi_cordic_if_RDATA : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_cordic_if_RRESP : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_cordic_if_RVALID : out STD_LOGIC;
    s_axi_cordic_if_RREADY : in STD_LOGIC;
    aclk : in STD_LOGIC;
    aresetn : in STD_LOGIC
  );

end uebung08_cordiccc_top_0_0;

architecture stub of uebung08_cordiccc_top_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "s_axi_cordic_if_AWADDR[5:0],s_axi_cordic_if_AWVALID,s_axi_cordic_if_AWREADY,s_axi_cordic_if_WDATA[31:0],s_axi_cordic_if_WSTRB[3:0],s_axi_cordic_if_WVALID,s_axi_cordic_if_WREADY,s_axi_cordic_if_BRESP[1:0],s_axi_cordic_if_BVALID,s_axi_cordic_if_BREADY,s_axi_cordic_if_ARADDR[5:0],s_axi_cordic_if_ARVALID,s_axi_cordic_if_ARREADY,s_axi_cordic_if_RDATA[31:0],s_axi_cordic_if_RRESP[1:0],s_axi_cordic_if_RVALID,s_axi_cordic_if_RREADY,aclk,aresetn";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "cordiccc_top,Vivado 2019.1";
begin
end;
