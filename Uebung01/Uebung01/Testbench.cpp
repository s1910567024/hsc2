#include "Testbench.h"
#include <vector>
#include <cmath>
#include <iomanip>

/***************************************************************************************************
* NOTES:
* Within the testbench sine & cosine (Cordic's X and Y) are compared with sine & cosine functions 
* from the cmath library. Our Test results show, that the absolute error with N=16 is <= 2^(-14).
* It turned out, that increasing N=18 will result in a absolute error <= 2^(-16). 
****************************************************************************************************/

SC_HAS_PROCESS(Testbench);

void Testbench::doTest()
{
	const double pi = 4*std::atan(1);
	
	const int cMaxIterations = 16;
	
	// absolute error
	const double e = std::pow(2, (-1)*(cMaxIterations));

	std::vector<double> testcases;

	// edge cases
	testcases.push_back(pi / 2);
	testcases.push_back(pi / 4);

	// granular cases (0.5 degree step size)
	for (unsigned int i = 0; i <= 20; i++)
	{
		testcases.push_back((double)i / 20  * pi / 180);
	}

	// granular cases (2^-16 degree step size)
	// NOTE: test cases with step size = 2^(-22) do
	// not make sense here, since the output is 16 Bit
	for (unsigned int i = 0; i <= 20; i++)
	{
		testcases.push_back(i * std::pow(2, -16));
	}

	for(size_t i = 0; i < testcases.size(); ++i)
	{

		oStart = false;
		oPhi = testcases[i];

		wait(1, SC_NS);
		oStart = true;

		wait(iRdy.posedge_event());

		// calculate cmath cos & sin
		double cmathCos = cos(testcases[i]);
		double cmathSin = sin(testcases[i]);

		// cordic cos (=iX) & sin (=iY)
		double cordicCos = iX.read();
		double cordicSin = iY.read();

		// absolute error
		double difCos = (std::abs(cmathCos - cordicCos));
		double difSin = (std::abs(cmathSin - cordicSin));

		// calculate phi from Cordic
		double phiCordic = std::atan2(iY.read(), iX.read());

		double phiTest = testcases[i];

		// print result
		std::cout << std::setprecision(16) << std::fixed << std::right 
		<< "Testing " << std::setw(20) << phiTest * 180 / pi  << "\370"
		<< ", Cordic Phi= " << phiCordic * 180 / pi << "\370" << std::endl;
			
		// check abolute error
		if (difCos > e || difSin > e)
		{
			std::cout << "Error result! Testcase #" << i << ":" << phiTest << std::endl;
		}
	}
}