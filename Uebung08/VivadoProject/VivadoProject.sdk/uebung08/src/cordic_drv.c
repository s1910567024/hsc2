#include "cordic_drv.h"
#include <math.h>
#include <stdbool.h>
#include "xstatus.h"
#include "xscugic.h"
#include "xparameters.h"
#include "xcordiccc.h"
#include "xil_exception.h"
#include <stdint.h>

#define M_PI 3.14159265358979323846

CordicStatus_t CordicCalcXY(float const* const phi, float* const cos, float* const sin, unsigned int* adr)
{
	float bPhi = *phi-((int)(*phi/2/M_PI))*2.0*M_PI;
	//float bPhi = fmod(*phi, 2*M_PI); //Between -2pi and 2*pi
	if (bPhi < 0)
		bPhi += 2 * M_PI;//Between 0 and 2*pi

	unsigned int data = (unsigned int)((*phi-((int)(*phi*2/M_PI))/2.0*M_PI) * (1<<21));

	CordicWrPhi(*adr, data);

	WaitForIrq();

	data = CordicRdXY(*adr);

	*cos = (data >> 16) * 1.0 / (1 << 16);
	*sin = (data & ((1 << 16) - 1)) * 1.0 / (1 << 16);

	if(bPhi < M_PI / 2) //1.Quadrant
	{
	}
	else if(bPhi < 2* M_PI / 2) //2.Quadrant
	{
		float tmp = *cos;
		*cos = -*sin;
		*sin = tmp;
	}
	else if (bPhi < 3 * M_PI / 2) //3.Quadrant
	{
		*cos = -*cos;
		*sin = -*sin;
	}
	else //4.Quadrant
	{
		float tmp = *sin;
		*sin = -*cos;
		*cos = tmp;
	}

	return Cordic_Status_OK;
}

CordicStatus_t cordic_init()
{
	return hal_cordic_init();
}
