#pragma once

#include "mySystemc.h"

#include <tlm.h>
#include <tlm_utils/simple_initiator_socket.h>
#include <tlm_utils/simple_target_socket.h>

#include "Cordic.h"
#include "offsets.h"

SC_MODULE(CordicTarget) {
public:
	//target socket using default settings
	tlm_utils::simple_target_socket<CordicTarget> socket;

	// CTOR
	SC_HAS_PROCESS(CordicTarget);

	CordicTarget(sc_module_name instance) : socket("bus_rw")
	{
		cordic = new Cordic("Cordic");
		cordic->iPhi(Phi);
		cordic->iStart(Start);
		cordic->oRdy(Rdy);
		cordic->oX(X);
		cordic->oY(Y);

		socket.register_b_transport(this, &CordicTarget::b_transport);
	}

	virtual void b_transport(tlm::tlm_generic_payload & trans, sc_time & delay) {
		// read parameters of transaction object
		tlm::tlm_command cmd = trans.get_command();
		sc_dt::uint64    adr = trans.get_address() / 4;
		unsigned char* ptr = trans.get_data_ptr();
		unsigned int     len = trans.get_data_length();
		unsigned char* byt = trans.get_byte_enable_ptr();
		unsigned int     wid = trans.get_streaming_width();

		// decode transaction and check parameters
		if (adr > 0x8) {
			trans.set_response_status(tlm::TLM_ADDRESS_ERROR_RESPONSE);
			SC_REPORT_ERROR("TLM-2", "Target does not support given generic payload transaction");
			return;
		}

		if (byt != 0) {
			trans.set_response_status(tlm::TLM_BYTE_ENABLE_ERROR_RESPONSE);
			return;
		}

		if (len > 4 || wid < len) {
			trans.set_response_status(tlm::TLM_BURST_ERROR_RESPONSE);
			return;
		}

		if(adr == OFFSET_CTL/4)//Config
		{
			if (cmd == tlm::TLM_READ_COMMAND) {
				*reinterpret_cast<int*>(ptr) = Rdy.read();
				Start = false;
			}
			else if (cmd == tlm::TLM_WRITE_COMMAND) {
				trans.set_response_status(tlm::TLM_COMMAND_ERROR_RESPONSE);
				return;
			}
		}
		else if(adr == OFFSET_PHI/4)//Phi
		{
			if (cmd == tlm::TLM_READ_COMMAND) {
				trans.set_response_status(tlm::TLM_COMMAND_ERROR_RESPONSE);
				return;
			}
			else if (cmd == tlm::TLM_WRITE_COMMAND) {
				sc_ufixed<22, 1> tmp;
				tmp.range(21, 0) = sc_uint<22>(*reinterpret_cast<int*>(ptr));
				Phi = tmp;
				Start = true;
			}
		}
		else if(adr == OFFSET_XY/4)//XY
		{
			if (cmd == tlm::TLM_READ_COMMAND) {
				*reinterpret_cast<int*>(ptr) = (sc_uint<16>(X.read().range(15, 0)).value() << 16) | sc_uint<16>(Y.read().range(15, 0)).value();
				Start = false;
			}
			else if (cmd == tlm::TLM_WRITE_COMMAND) {
				trans.set_response_status(tlm::TLM_COMMAND_ERROR_RESPONSE);
				return;
			}
		}

		trans.set_response_status(tlm::TLM_OK_RESPONSE);
	}

public: //public for trace
	sc_signal<bool> Start;
	sc_signal<bool> Rdy;
	sc_signal<sc_ufixed<22, 1>> Phi;
	sc_signal<sc_ufixed<16, 0, SC_RND, SC_SAT>> X;
	sc_signal<sc_ufixed<16, 0, SC_RND, SC_SAT>> Y;

private:
	Cordic * cordic;
};