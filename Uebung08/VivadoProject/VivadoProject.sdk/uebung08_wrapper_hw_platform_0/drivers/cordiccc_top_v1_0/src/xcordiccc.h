// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef XCORDICCC_H
#define XCORDICCC_H

#ifdef __cplusplus
extern "C" {
#endif

/***************************** Include Files *********************************/
#ifndef __linux__
#include "xil_types.h"
#include "xil_assert.h"
#include "xstatus.h"
#include "xil_io.h"
#else
#include <stdint.h>
#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stddef.h>
#endif
#include "xcordiccc_hw.h"

/**************************** Type Definitions ******************************/
#ifdef __linux__
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
#else
typedef struct {
    u16 DeviceId;
    u32 Cordic_if_BaseAddress;
} XCordiccc_Config;
#endif

typedef struct {
    u32 Cordic_if_BaseAddress;
    u32 IsReady;
} XCordiccc;

/***************** Macros (Inline Functions) Definitions *********************/
#ifndef __linux__
#define XCordiccc_WriteReg(BaseAddress, RegOffset, Data) \
    Xil_Out32((BaseAddress) + (RegOffset), (u32)(Data))
#define XCordiccc_ReadReg(BaseAddress, RegOffset) \
    Xil_In32((BaseAddress) + (RegOffset))
#else
#define XCordiccc_WriteReg(BaseAddress, RegOffset, Data) \
    *(volatile u32*)((BaseAddress) + (RegOffset)) = (u32)(Data)
#define XCordiccc_ReadReg(BaseAddress, RegOffset) \
    *(volatile u32*)((BaseAddress) + (RegOffset))

#define Xil_AssertVoid(expr)    assert(expr)
#define Xil_AssertNonvoid(expr) assert(expr)

#define XST_SUCCESS             0
#define XST_DEVICE_NOT_FOUND    2
#define XST_OPEN_DEVICE_FAILED  3
#define XIL_COMPONENT_IS_READY  1
#endif

/************************** Function Prototypes *****************************/
#ifndef __linux__
int XCordiccc_Initialize(XCordiccc *InstancePtr, u16 DeviceId);
XCordiccc_Config* XCordiccc_LookupConfig(u16 DeviceId);
int XCordiccc_CfgInitialize(XCordiccc *InstancePtr, XCordiccc_Config *ConfigPtr);
#else
int XCordiccc_Initialize(XCordiccc *InstancePtr, const char* InstanceName);
int XCordiccc_Release(XCordiccc *InstancePtr);
#endif


void XCordiccc_SetIstart(XCordiccc *InstancePtr, u32 Data);
u32 XCordiccc_GetIstart(XCordiccc *InstancePtr);
u32 XCordiccc_GetOrdy(XCordiccc *InstancePtr);
void XCordiccc_SetIphi(XCordiccc *InstancePtr, u32 Data);
u32 XCordiccc_GetIphi(XCordiccc *InstancePtr);
u32 XCordiccc_GetOx(XCordiccc *InstancePtr);
u32 XCordiccc_GetOy(XCordiccc *InstancePtr);

#ifdef __cplusplus
}
#endif

#endif
