// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================
/***************************** Include Files *********************************/
#include "xcordiccc.h"

/************************** Function Implementation *************************/
#ifndef __linux__
int XCordiccc_CfgInitialize(XCordiccc *InstancePtr, XCordiccc_Config *ConfigPtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(ConfigPtr != NULL);

    InstancePtr->Cordic_if_BaseAddress = ConfigPtr->Cordic_if_BaseAddress;
    InstancePtr->IsReady = XIL_COMPONENT_IS_READY;

    return XST_SUCCESS;
}
#endif

void XCordiccc_SetIstart(XCordiccc *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XCordiccc_WriteReg(InstancePtr->Cordic_if_BaseAddress, XCORDICCC_CORDIC_IF_ADDR_ISTART_DATA, Data);
}

u32 XCordiccc_GetIstart(XCordiccc *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XCordiccc_ReadReg(InstancePtr->Cordic_if_BaseAddress, XCORDICCC_CORDIC_IF_ADDR_ISTART_DATA);
    return Data;
}

u32 XCordiccc_GetOrdy(XCordiccc *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XCordiccc_ReadReg(InstancePtr->Cordic_if_BaseAddress, XCORDICCC_CORDIC_IF_ADDR_ORDY_DATA);
    return Data;
}

void XCordiccc_SetIphi(XCordiccc *InstancePtr, u32 Data) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XCordiccc_WriteReg(InstancePtr->Cordic_if_BaseAddress, XCORDICCC_CORDIC_IF_ADDR_IPHI_DATA, Data);
}

u32 XCordiccc_GetIphi(XCordiccc *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XCordiccc_ReadReg(InstancePtr->Cordic_if_BaseAddress, XCORDICCC_CORDIC_IF_ADDR_IPHI_DATA);
    return Data;
}

u32 XCordiccc_GetOx(XCordiccc *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XCordiccc_ReadReg(InstancePtr->Cordic_if_BaseAddress, XCORDICCC_CORDIC_IF_ADDR_OX_DATA);
    return Data;
}

u32 XCordiccc_GetOy(XCordiccc *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XCordiccc_ReadReg(InstancePtr->Cordic_if_BaseAddress, XCORDICCC_CORDIC_IF_ADDR_OY_DATA);
    return Data;
}

