#pragma once

typedef struct {
	unsigned short DeviceId;		/* Unique ID  of device */
	unsigned int * BaseAddress;	/* Device base address */
} XCordic_Config;

typedef struct {
	unsigned int * BaseAddress;	/* Device base address */
	unsigned int IsReady;		/* Device is initialized and ready */
} XCordic;

int XCordic_CfgInitialize(XCordic *InstancePtr, const XCordic_Config *ConfigPtr);

XCordic_Config *XCordic_LookupConfig(short DeviceId);

int XCordic_Initialize(XCordic *InstancePtr, short DeviceId);

void XCordic_InterruptClear(XCordic * ptr);
void XCordic_InterruptEnable(XCordic * ptr);