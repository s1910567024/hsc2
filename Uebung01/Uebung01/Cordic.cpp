#include "Cordic.h"
#include <cmath>

SC_HAS_PROCESS(Cordic);

void Cordic::doWork()
{
	oRdy = true;

	while(true)
	{
		wait(iStart.posedge_event());

		oRdy = false;

		double x = 1.0;
		double y = 0.0;

		double z = iPhi.read();
		double K = 1;
		const int N = 18; // by increasing N, the absolute error is minimized by LSB

		for (int n = 0; n < N; ++n)
		{
			//compute direction of rotation
			int d = z < 0 ? -1 : 1;

			//calculate gain
			K = K * 1 / std::sqrt(1 + std::pow(2, -2 * n));

			//compute next z
			z = z - std::atan(std::pow(2, -n)) * d;

			//calculate x and y
			double newX = x - d * std::pow(2, -n) * y;
			double newY = d * std::pow(2, -n) * x + y;

			x = newX;
			y = newY;
		}

		//set output
		oX = x * K;
		oY = y * K;

		wait(SC_ZERO_TIME);

		oRdy = true;
	}
}