#pragma once

#include "mySystemc.h"

#include <tlm.h>
#include <tlm_utils/simple_initiator_socket.h>
#include <tlm_utils/simple_target_socket.h>

#include "Cordic.h"
#include "offsets.h"
#include "CordicCC.h"
#include "ResetGenerator.h"

SC_MODULE(CordicTarget) {
public:
	//target socket using default settings
	tlm_utils::simple_target_socket<CordicTarget> socket;

	// CTOR
	SC_HAS_PROCESS(CordicTarget);

	CordicTarget(sc_module_name instance) : socket("bus_rw")
	{
		cordic = new Cordic("Cordic");
		cordic->iPhi(Phi);
		cordic->iStart(Start);
		cordic->oRdy(Rdy);
		cordic->oX(X);
		cordic->oY(Y);

		cordicCC = new CordicCC("CordicCC");
		cordicCC->clk(clk);
		cordicCC->nrst(nRst);
		cordicCC->iPhi(Phi);
		cordicCC->iStart(Start);
		cordicCC->oRdy(RdyCC);
		cordicCC->oX(XCC);
		cordicCC->oY(YCC);

		rg = new ResetGenerator("ResetGenerator");
		rg->nrst(nRst);

		socket.register_b_transport(this, &CordicTarget::b_transport);

		SC_METHOD(IrqMethod);
		sensitive << RdyCC;
	}

	void IrqMethod()
	{
		oIrq = RdyCC.read();
	}

	virtual void b_transport(tlm::tlm_generic_payload & trans, sc_time & delay) {
		// read parameters of transaction object
		tlm::tlm_command cmd = trans.get_command();
		sc_dt::uint64    adr = trans.get_address() / 4;
		unsigned char* ptr = trans.get_data_ptr();
		unsigned int     len = trans.get_data_length();
		unsigned char* byt = trans.get_byte_enable_ptr();
		unsigned int     wid = trans.get_streaming_width();

		// decode transaction and check parameters
		if (adr > 0x8) {
			trans.set_response_status(tlm::TLM_ADDRESS_ERROR_RESPONSE);
			SC_REPORT_ERROR("TLM-2", "Target does not support given generic payload transaction");
			return;
		}

		if (byt != 0) {
			trans.set_response_status(tlm::TLM_BYTE_ENABLE_ERROR_RESPONSE);
			return;
		}

		if (len > 4 || wid < len) {
			trans.set_response_status(tlm::TLM_BURST_ERROR_RESPONSE);
			return;
		}

		if(adr == OFFSET_CTL/4)//Config
		{
			if (cmd == tlm::TLM_READ_COMMAND) {
				*reinterpret_cast<int*>(ptr) = Rdy.read() && RdyCC.read() ? 1 : 0;
			}
			else if (cmd == tlm::TLM_WRITE_COMMAND) {
				trans.set_response_status(tlm::TLM_COMMAND_ERROR_RESPONSE);
				return;
			}
		}
		else if(adr == OFFSET_PHI/4)//Phi
		{
			if (cmd == tlm::TLM_READ_COMMAND) {
				trans.set_response_status(tlm::TLM_COMMAND_ERROR_RESPONSE);
				return;
			}
			else if (cmd == tlm::TLM_WRITE_COMMAND) {
				sc_ufixed<22, 1> tmp;
				tmp.range(21, 0) = sc_uint<22>(*reinterpret_cast<int*>(ptr));
				Phi = tmp;
				Start = true;
				wait(RdyCC.negedge_event());
				Start = false;
			}
		}
		else if(adr == OFFSET_XY/4)//XY
		{
			if (cmd == tlm::TLM_READ_COMMAND) {
				if (X.read() != XCC.read() || Y.read() != YCC.read())
				{
					if(abs(X.read() - XCC.read()) > std::pow(2,-14))
					{
						std::cout << std::fixed << "X diff: " << X.read() - XCC.read() << std::endl;
					}
					if (abs(Y.read() - YCC.read()) > std::pow(2, -14))
					{
						std::cout << std::fixed << "Y diff: " << Y.read() - YCC.read() << std::endl;
					}
					/*std::cout << X.read() << std::endl;
					std::cout << XCC.read() << std::endl;

					std::cout << Y.read() << std::endl;
					std::cout << YCC.read() << std::endl;*/
				}

				*reinterpret_cast<int*>(ptr) = (sc_uint<16>(X.read().range(15, 0)).value() << 16) | sc_uint<16>(Y.read().range(15, 0)).value();
			}
			else if (cmd == tlm::TLM_WRITE_COMMAND) {
				trans.set_response_status(tlm::TLM_COMMAND_ERROR_RESPONSE);
				return;
			}
		}

		trans.set_response_status(tlm::TLM_OK_RESPONSE);
	}

public: //public for trace
	sc_out<bool> oIrq;
	sc_clock clk{ "clk", 10, SC_NS };
	sc_signal<bool> nRst;
	sc_signal<bool> Start;
	sc_signal<bool> Rdy;
	sc_signal<bool> RdyCC;
	sc_signal<sc_ufixed<22, 1>> Phi;
	sc_signal<sc_ufixed<16, 0, SC_RND, SC_SAT>> X;
	sc_signal<sc_ufixed<16, 0, SC_RND, SC_SAT>> Y;
	sc_signal<sc_ufixed<16, 0, SC_RND, SC_SAT>> XCC;
	sc_signal<sc_ufixed<16, 0, SC_RND, SC_SAT>> YCC;

private:
	Cordic* cordic;
	CordicCC* cordicCC;
	ResetGenerator * rg;
};