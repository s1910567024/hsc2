#pragma once

#include <systemc.h>
#include "Tests.h"

SC_MODULE(MasterCC)
{
	sc_in<bool> iClk, iRst;
	sc_out<int> oAdr;
	sc_out<int> oDat;
	sc_out<int> oSel;
	sc_out<bool> oCyc, oStb, oWe;
	sc_in<int> iDat;
	sc_in<bool> iAck;

	SC_CTOR(MasterCC)
	{
		SC_THREAD(doWork);
	}

	void singleWrite(int const & addr, int const & data)
	{
		wait(iClk.posedge_event());

		oAdr = addr;
		oDat = data;
		oWe = true;
		oSel = 3;
		oStb = true;
		oCyc = true;

		do
		{
			wait(iClk.posedge_event());
		} while (iAck == false);

		oStb = false;
		oCyc = false;

		wait(iClk.posedge_event());
	}

	int singleRead(int const& addr)
	{
		int data;
		wait(iClk.posedge_event());

		oAdr = addr;
		oWe = false;
		oSel = 3;
		oStb = true;
		oCyc = true;

		do
		{
			wait(iClk.posedge_event());
		} while (iAck == false);

		data = iDat;
		oStb = false;
		oCyc = false;

		wait(iClk.posedge_event());

		return data;
	}

private:

	void doWork()
	{
		doTests(*this);
	}
};