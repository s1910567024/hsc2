#pragma once

#include <systemc.h>
using namespace sc_core;
using namespace sc_dt;
using namespace std;

#include <tlm.h>
#include <tlm_utils/simple_initiator_socket.h>
#include <tlm_utils/simple_target_socket.h>

#include "MasterCC.h"
#include "MemoryCC.h"
#include "MasterLT.h"
#include "MemoryLT.h"

template<bool gIsCC>
SC_MODULE(top) {

	sc_clock clk{ "clk", 10, SC_NS };
	sc_signal<bool> rst;

	MasterCC* mMasterCC;
	MemoryCC* mMemoryCC;

	sc_signal<bool> Clk, Rst;
	sc_signal<int> Adr;
	sc_signal<int> Dat;
	sc_signal<int> Sel;
	sc_signal<bool> Cyc, Stb, We;
	sc_signal<int> Dat2;
	sc_signal<bool> Ack;


	MasterLT* mMasterLT;
	MemoryLT* mMemoryLT;

	SC_CTOR(top) {
		if (gIsCC == true)
		{
			mMasterCC = new MasterCC("initiatorCC");
			mMemoryCC = new MemoryCC("targetCC");

			mMasterCC->iClk(clk);
			mMasterCC->iRst(rst);
			mMasterCC->oAdr(Adr);
			mMasterCC->oDat(Dat);
			mMasterCC->oSel(Sel);
			mMasterCC->oCyc(Cyc);
			mMasterCC->oStb(Stb);
			mMasterCC->oWe(We);
			mMasterCC->iDat(Dat2);
			mMasterCC->iAck(Ack);

			mMemoryCC->iClk(clk);
			mMemoryCC->iRst(rst);
			mMemoryCC->iAdr(Adr);
			mMemoryCC->iDat(Dat);
			mMemoryCC->iSel(Sel);
			mMemoryCC->iCyc(Cyc);
			mMemoryCC->iStb(Stb);
			mMemoryCC->iWe(We);
			mMemoryCC->oDat(Dat2);
			mMemoryCC->oAck(Ack);
		}
		else
		{
			mMasterLT = new MasterLT("initiatorLT");
			mMemoryLT = new MemoryLT("targetLT");

			mMasterLT->socket.bind(mMemoryLT->socket);
		}
	}
};