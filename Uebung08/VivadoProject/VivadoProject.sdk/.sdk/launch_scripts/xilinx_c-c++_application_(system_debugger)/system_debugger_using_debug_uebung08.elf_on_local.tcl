connect -url tcp:127.0.0.1:3121
source E:/2Semester/HSC2/hsc2/Uebung08/VivadoProject/VivadoProject.srcs/sources_1/bd/uebung08/ip/uebung08_processing_system7_0_2/ps7_init.tcl
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Avnet MiniZed V1 1234-oj1A"} -index 0
rst -system
after 3000
targets -set -filter {jtag_cable_name =~ "Avnet MiniZed V1 1234-oj1A" && level==0} -index 1
fpga -file E:/2Semester/HSC2/hsc2/Uebung08/VivadoProject/VivadoProject.sdk/hw_platform_0/uebung08_wrapper.bit
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Avnet MiniZed V1 1234-oj1A"} -index 0
loadhw -hw E:/2Semester/HSC2/hsc2/Uebung08/VivadoProject/VivadoProject.sdk/hw_platform_0/system.hdf -mem-ranges [list {0x40000000 0xbfffffff}]
configparams force-mem-access 1
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Avnet MiniZed V1 1234-oj1A"} -index 0
ps7_init
ps7_post_config
targets -set -nocase -filter {name =~ "ARM*#0" && jtag_cable_name =~ "Avnet MiniZed V1 1234-oj1A"} -index 0
dow E:/2Semester/HSC2/hsc2/Uebung08/VivadoProject/VivadoProject.sdk/uebung08/Debug/uebung08.elf
configparams force-mem-access 0
bpadd -addr &main
