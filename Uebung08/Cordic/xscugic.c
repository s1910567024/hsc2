#include "xscugic.h"

void Xil_ExceptionEnable()
{
}

void XScuGic_Enable(XScuGic* InstancePtr, unsigned int Int_Id)
{
}

int XScuGic_Connect(XScuGic *InstancePtr, unsigned int Int_Id, Xil_InterruptHandler Handler, void *CallBackRef)
{
	return 0;
}

void Xil_ExceptionRegisterHandler(unsigned int Exception_id, Xil_ExceptionHandler Handler, void* Data)
{
}

void Xil_ExceptionInit()
{
}

int XScuGic_CfgInitialize(XScuGic *InstancePtr, XScuGic_Config *ConfigPtr, unsigned int EffectiveAddr)
{
	return 0;
}

XScuGic_Config *XScuGic_LookupConfig(unsigned short DeviceId)
{
	return 0;
}

void XScuGic_InterruptHandler(XScuGic *InstancePtr)
{
}
