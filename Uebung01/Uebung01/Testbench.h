#pragma once

#include "mySystemc.h"

SC_MODULE(Testbench)
{
	sc_out<bool> oStart;
	sc_in<bool> iRdy;
	sc_out<sc_ufixed<22, 1>> oPhi;
	sc_in<sc_ufixed<16, 0, SC_RND, SC_SAT>> iX;
	sc_in<sc_ufixed<16, 0, SC_RND, SC_SAT>> iY;

	SC_CTOR(Testbench)
	{
		SC_THREAD(doTest);
	}

private:
	void doTest();
};