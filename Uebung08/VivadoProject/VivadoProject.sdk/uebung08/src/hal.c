#include "hal.h"
#include "offsets.h"
#include <stdbool.h>

#include "xCordiccc.h"

#define INTC_DEVICE_ID			XPAR_SCUGIC_SINGLE_DEVICE_ID
#define CORDIC_DEVICE_ID		XPAR_CORDICCC_TOP_0_DEVICE_ID
#define CORDIC_IRPT_INTR		XPAR_CORDICCC_INTR

static XCordiccc CordicInstance;

extern void write_bus(unsigned int adr, unsigned int data);
extern void read_bus(unsigned int adr, unsigned int * data);
extern void wait_irq();

unsigned int CordicRdCtl(unsigned int adr)
{
#ifdef EMUCPUINSTANCE
	unsigned int data;
	read_bus(adr + OFFSET_CTL, &data);
	return data;
#else
	return XCordiccc_GetOrdy(&CordicInstance);
#endif
}

void CordicWrPhi(unsigned int adr, unsigned int data)
{
#ifdef EMUCPUINSTANCE
	write_bus(adr + OFFSET_PHI, data);
#else
	XCordiccc_SetIphi(&CordicInstance, data);
	XCordiccc_SetIstart(&CordicInstance, 1);
	XCordiccc_SetIstart(&CordicInstance, 0);
#endif
}

unsigned int CordicRdXY(unsigned int adr)
{
#ifdef EMUCPUINSTANCE
	unsigned int data;
	read_bus(adr + OFFSET_XY, &data);
	return data;
#else
	return XCordiccc_GetOx(&CordicInstance) << 16 | XCordiccc_GetOy(&CordicInstance);
#endif
}

CordicStatus_t hal_cordic_init()
{
	return XCordiccc_Initialize(&CordicInstance, CORDIC_DEVICE_ID);
}

void WaitForIrq()
{
#ifdef EMUCPUINSTANCE
	wait_irq();
#else
	int test;
	do
	{
		test = XCordiccc_GetOrdy(&CordicInstance);
	} while((test&1) == 0);
#endif
}
