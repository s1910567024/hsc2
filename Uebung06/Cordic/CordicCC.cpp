#include "CordicCC.h"
#include <cmath>

SC_HAS_PROCESS(CordicCC);

static const sc_ufixed<22, 0> atanArray[] =
{
	0.78539816339744827899949086713605,
	0.46364760900080609351547877849953,
	0.24497866312686414347332686247682,
	0.12435499454676143815667899161781,
	0.06241880999595735002305474381501,
	0.03123983343026827744215445648024,
	0.01562372862047683129416153491320,
	0.00781234106010111114398730691732,
	0.00390623013196697175739013907503,
	0.00195312251647881875843415500071,
	0.00097656218955931945943649274966,
	0.00048828121119489828992621394121,
	0.00024414062014936177124474481204,
	0.00012207031189367020785306594544,
	0.00006103515617420877259350145416,
	0.00003051757811552609572715473452
};

void CordicCC::doWork()
{
	oRdy = true;
	wait();

	while (true)
	{
		while(iStart == false)
		{
			wait();
		}
		//wait(iStart.posedge_event());

		oRdy = false;

		sc_fixed<22, 2, SC_RND, SC_SAT> x = 1.0;
		sc_fixed<22, 2, SC_RND, SC_SAT> y = 0.0;

		sc_fixed<27, 2> z = iPhi.read();
		const int N = 16; // by increasing N, the absolute error is minimized by LSB
		sc_fixed<18, 1, SC_RND, SC_SAT> K = 0.60725293510313926859112143574748;

		for (int n = 0; n < N; ++n)
		{
			//compute direction of rotation
			int d = z < 0 ? -1 : 1;

			//compute next z
			z = z - atanArray[n] * d;

			//calculate x and y
			sc_fixed<22, 2, SC_RND, SC_SAT> newX = x - y * d / (1 << n);
			sc_fixed<22, 2, SC_RND, SC_SAT> newY = x * d / (1 << n) + y;

			x = newX;
			y = newY;

			wait();
		}

		//set output
		oX = x * K;
		oY = y * K;

		oRdy = true;
	}
}