#include "hls_design_meta.h"
const Port_Property HLS_Design_Meta::port_props[]={
	Port_Property("clk", 1, hls_in, -1, "", "", 1),
	Port_Property("nrst", 1, hls_in, -1, "", "", 1),
	Port_Property("iStart", 1, hls_in, 2, "ap_none", "in_data", 1),
	Port_Property("oRdy", 1, hls_out, 3, "ap_vld", "out_data", 1),
	Port_Property("iPhi", 22, hls_in, 4, "ap_none", "in_data", 1),
	Port_Property("oX", 16, hls_out, 5, "ap_vld", "out_data", 1),
	Port_Property("oY", 16, hls_out, 6, "ap_vld", "out_data", 1),
};
const char* HLS_Design_Meta::dut_name = "CordicCC::CordicCC";
