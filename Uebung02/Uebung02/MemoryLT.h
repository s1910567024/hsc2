#pragma once

#include <systemc.h>

#include <tlm.h>
#include <tlm_utils/simple_initiator_socket.h>
#include <tlm_utils/simple_target_socket.h>

SC_MODULE(MemoryLT) {
public:
	//target socket using default settings
	tlm_utils::simple_target_socket<MemoryLT> socket;

	// CTOR
	SC_HAS_PROCESS(MemoryLT);
	MemoryLT(sc_module_name instance) : socket("bus_rw"), LATENCY(10, SC_NS) {
		// register callback for incoming b_transport interface method call
		socket.register_b_transport(this, &MemoryLT::b_transport);

		// Initialize memory with random data
		for (int i = 0; i < SIZE; i++)
			mem[i] = 0xAA000000 | (rand() % 256);
	}

	virtual void b_transport(tlm::tlm_generic_payload& trans, sc_time& delay) {
		// read parameters of transaction object
		tlm::tlm_command cmd = trans.get_command();
		sc_dt::uint64    adr = trans.get_address() / 4;
		unsigned char* ptr = trans.get_data_ptr();
		unsigned int     len = trans.get_data_length();
		unsigned char* byt = trans.get_byte_enable_ptr();
		unsigned int     wid = trans.get_streaming_width();

		// decode transaction and check parameters
		if (adr >= sc_dt::uint64(SIZE)) {
			trans.set_response_status(tlm::TLM_ADDRESS_ERROR_RESPONSE);
			SC_REPORT_ERROR("TLM-2", "Target does not support given generic payload transaction");
			return;
		}

		if (byt != 0) {
			trans.set_response_status(tlm::TLM_BYTE_ENABLE_ERROR_RESPONSE);
			return;
		}

		if (len > 4 || wid < len) {
			trans.set_response_status(tlm::TLM_BURST_ERROR_RESPONSE);
			return;
		}

		// read or write data -> can be copied by memcpy or direct access via pointer
		if (cmd == tlm::TLM_READ_COMMAND) {
			// *ptr = mem[adr];				// in this case we need the typecast when reading the parameter
			memcpy(ptr, &mem[adr], len);
		}
		else if (cmd == tlm::TLM_WRITE_COMMAND) {
			// mem[adr] = *ptr;
			memcpy(&mem[adr], ptr, len);
		}


		// Set DMI hint to indicated that DMI is supported
		trans.set_dmi_allowed(true);

		// finaly set the response status attribute of the gerneric payload to
		// indicate successful and complete transaction
		trans.set_response_status(tlm::TLM_OK_RESPONSE);

		delay += sc_time(10, SC_NS) * mDist(mEngine);
	}

private:
	// size of memory
	enum { SIZE = 256 };
	// latency for direct memory access
	const sc_time LATENCY;
	// memory
	int mem[SIZE];

	std::mt19937 mEngine{ 0 };
	std::uniform_int_distribution<> mDist{ 1, 10 };
};