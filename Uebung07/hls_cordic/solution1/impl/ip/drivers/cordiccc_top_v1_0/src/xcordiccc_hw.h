// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================
// cordic_if
// 0x00 : reserved
// 0x04 : reserved
// 0x08 : reserved
// 0x0c : reserved
// 0x10 : reserved
// 0x14 : Data signal of iStart
//        bit 0  - iStart[0] (Read/Write)
//        others - reserved
// 0x18 : reserved
// 0x1c : Data signal of oRdy
//        bit 0  - oRdy[0] (Read)
//        others - reserved
// 0x20 : reserved
// 0x24 : Data signal of iPhi
//        bit 21~0 - iPhi[21:0] (Read/Write)
//        others   - reserved
// 0x28 : reserved
// 0x2c : Data signal of oX
//        bit 15~0 - oX[15:0] (Read)
//        others   - reserved
// 0x30 : reserved
// 0x34 : Data signal of oY
//        bit 15~0 - oY[15:0] (Read)
//        others   - reserved
// (SC = Self Clear, COR = Clear on Read, TOW = Toggle on Write, COH = Clear on Handshake)

#define XCORDICCC_CORDIC_IF_ADDR_ISTART_DATA 0x14
#define XCORDICCC_CORDIC_IF_BITS_ISTART_DATA 1
#define XCORDICCC_CORDIC_IF_ADDR_ORDY_DATA   0x1c
#define XCORDICCC_CORDIC_IF_BITS_ORDY_DATA   1
#define XCORDICCC_CORDIC_IF_ADDR_IPHI_DATA   0x24
#define XCORDICCC_CORDIC_IF_BITS_IPHI_DATA   22
#define XCORDICCC_CORDIC_IF_ADDR_OX_DATA     0x2c
#define XCORDICCC_CORDIC_IF_BITS_OX_DATA     16
#define XCORDICCC_CORDIC_IF_ADDR_OY_DATA     0x34
#define XCORDICCC_CORDIC_IF_BITS_OY_DATA     16

