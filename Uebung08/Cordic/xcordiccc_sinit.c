#include "xcordiccc.h"
#include "xparameters.h"
#include "xstatus.h"
#include <stdint.h>

XCordic_Config XCordic_ConfigTable[XPAR_XCORDIC_NUM_INSTANCES] =
{
	{
		XPAR_AXI_CORDIC_0_DEVICE_ID,
		XPAR_AXI_CORDIC_0_BASEADDR
	}
};

int XCordic_CfgInitialize(XCordic *InstancePtr, const XCordic_Config *ConfigPtr)
{
	int Status = XST_SUCCESS;

	InstancePtr->IsReady = 1;
	InstancePtr->BaseAddress = ConfigPtr->BaseAddress;

	return Status;
}

XCordic_Config *XCordic_LookupConfig(short DeviceId)
{
	XCordic_Config *ConfigPtr = NULL;

	int Index;

	for (Index = 0; Index < XPAR_XCORDIC_NUM_INSTANCES; Index++)
	{
		if (XCordic_ConfigTable[Index].DeviceId == DeviceId)
		{
			ConfigPtr = &XCordic_ConfigTable[Index];
			break;
		}
	}

	return ConfigPtr;
}

int XCordic_Initialize(XCordic *InstancePtr, short DeviceId)
{
	 XCordic_Config *ConfigPtr;

	 ConfigPtr = XCordic_LookupConfig(DeviceId);
	 if (ConfigPtr == NULL)
	 {
		 InstancePtr->IsReady = 0;
		 return (XST_DEVICE_NOT_FOUND);
	 }

	 return XCordic_CfgInitialize(InstancePtr, ConfigPtr);
}

void XCordic_InterruptClear(XCordic* ptr)
{
}

void XCordic_InterruptEnable(XCordic* ptr)
{
}
