// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef __linux__

#include "xstatus.h"
#include "xparameters.h"
#include "xcordiccc.h"

extern XCordiccc_Config XCordiccc_ConfigTable[];

XCordiccc_Config *XCordiccc_LookupConfig(u16 DeviceId) {
	XCordiccc_Config *ConfigPtr = NULL;

	int Index;

	for (Index = 0; Index < XPAR_XCORDICCC_NUM_INSTANCES; Index++) {
		if (XCordiccc_ConfigTable[Index].DeviceId == DeviceId) {
			ConfigPtr = &XCordiccc_ConfigTable[Index];
			break;
		}
	}

	return ConfigPtr;
}

int XCordiccc_Initialize(XCordiccc *InstancePtr, u16 DeviceId) {
	XCordiccc_Config *ConfigPtr;

	Xil_AssertNonvoid(InstancePtr != NULL);

	ConfigPtr = XCordiccc_LookupConfig(DeviceId);
	if (ConfigPtr == NULL) {
		InstancePtr->IsReady = 0;
		return (XST_DEVICE_NOT_FOUND);
	}

	return XCordiccc_CfgInitialize(InstancePtr, ConfigPtr);
}

#endif

