#ifndef TIMER_H
#define TIMER_H

int timer_init(void);

void timer_setLoadValue(u32 loadValue);

void timer_setCallback(void(*callback)(void));

void timer_start(void);

void timer_stop(void);

#endif
