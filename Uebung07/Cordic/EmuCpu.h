#pragma once
#include "mySystemc.h"

using namespace sc_core;
using namespace sc_dt;
using namespace std;

#include <memory>
#include <tlm.h>
#include <tlm_utils/simple_initiator_socket.h>
#include <tlm_utils/simple_target_socket.h>

class EmuCpu : sc_module
{
private:
	static std::unique_ptr<EmuCpu> instance;
	int (*main_c)(void);

public:
	tlm_utils::simple_initiator_socket<EmuCpu> socket;
	sc_in<bool> iIrq;

private:
	typedef EmuCpu SC_CURRENT_USER_MODULE;
	EmuCpu(sc_module_name name, int (*main)(void));

	void run();

	void doTransaction(tlm::tlm_command cmd, unsigned int adr, unsigned int& data);

public:
	EmuCpu(EmuCpu const&) = delete;
	EmuCpu(EmuCpu&&) = delete;
	EmuCpu& operator=(EmuCpu const&) = delete;
	EmuCpu& operator=(EmuCpu&&) = delete;
	~EmuCpu() = default;

	static EmuCpu& getInstance(char const* name, int (*main)(void));

	static EmuCpu& getInstance();

	void write_bus(unsigned int adr, unsigned int data);

	void read_bus(unsigned int adr, unsigned int& data);

	void wait_irq();
};