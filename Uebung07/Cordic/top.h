#pragma once

#include "mySystemc.h"
using namespace sc_core;
using namespace sc_dt;
using namespace std;

#include <tlm.h>
#include <tlm_utils/simple_initiator_socket.h>
#include <tlm_utils/simple_target_socket.h>

#include "EmuCpu.h"
#include "CordicTarget.h"
extern "C"
{
	#include "main_c.h"
}

SC_MODULE(top) {
	EmuCpu* mEmuCpu;
	CordicTarget* mCordicTarget;

	SC_CTOR(top) {
		mEmuCpu = &EmuCpu::getInstance("EmuCpu", main_c);
		mCordicTarget = new CordicTarget("CordicTarget");

		mEmuCpu->iIrq(IRQ);
		mCordicTarget->oIrq(IRQ);
		mEmuCpu->socket.bind(mCordicTarget->socket);
	}

	sc_signal<bool> IRQ;
};