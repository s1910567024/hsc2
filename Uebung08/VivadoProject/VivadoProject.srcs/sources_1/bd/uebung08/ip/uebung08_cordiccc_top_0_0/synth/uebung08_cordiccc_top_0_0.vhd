-- (c) Copyright 1995-2020 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: xilinx.com:hls:cordiccc_top:1.0
-- IP Revision: 2007070940

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY uebung08_cordiccc_top_0_0 IS
  PORT (
    s_axi_cordic_if_AWADDR : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    s_axi_cordic_if_AWVALID : IN STD_LOGIC;
    s_axi_cordic_if_AWREADY : OUT STD_LOGIC;
    s_axi_cordic_if_WDATA : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axi_cordic_if_WSTRB : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axi_cordic_if_WVALID : IN STD_LOGIC;
    s_axi_cordic_if_WREADY : OUT STD_LOGIC;
    s_axi_cordic_if_BRESP : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axi_cordic_if_BVALID : OUT STD_LOGIC;
    s_axi_cordic_if_BREADY : IN STD_LOGIC;
    s_axi_cordic_if_ARADDR : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    s_axi_cordic_if_ARVALID : IN STD_LOGIC;
    s_axi_cordic_if_ARREADY : OUT STD_LOGIC;
    s_axi_cordic_if_RDATA : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axi_cordic_if_RRESP : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axi_cordic_if_RVALID : OUT STD_LOGIC;
    s_axi_cordic_if_RREADY : IN STD_LOGIC;
    aclk : IN STD_LOGIC;
    aresetn : IN STD_LOGIC
  );
END uebung08_cordiccc_top_0_0;

ARCHITECTURE uebung08_cordiccc_top_0_0_arch OF uebung08_cordiccc_top_0_0 IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : STRING;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF uebung08_cordiccc_top_0_0_arch: ARCHITECTURE IS "yes";
  COMPONENT cordiccc_top IS
    GENERIC (
      C_S_AXI_CORDIC_IF_ADDR_WIDTH : INTEGER;
      C_S_AXI_CORDIC_IF_DATA_WIDTH : INTEGER
    );
    PORT (
      s_axi_cordic_if_AWADDR : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
      s_axi_cordic_if_AWVALID : IN STD_LOGIC;
      s_axi_cordic_if_AWREADY : OUT STD_LOGIC;
      s_axi_cordic_if_WDATA : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      s_axi_cordic_if_WSTRB : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      s_axi_cordic_if_WVALID : IN STD_LOGIC;
      s_axi_cordic_if_WREADY : OUT STD_LOGIC;
      s_axi_cordic_if_BRESP : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      s_axi_cordic_if_BVALID : OUT STD_LOGIC;
      s_axi_cordic_if_BREADY : IN STD_LOGIC;
      s_axi_cordic_if_ARADDR : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
      s_axi_cordic_if_ARVALID : IN STD_LOGIC;
      s_axi_cordic_if_ARREADY : OUT STD_LOGIC;
      s_axi_cordic_if_RDATA : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      s_axi_cordic_if_RRESP : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      s_axi_cordic_if_RVALID : OUT STD_LOGIC;
      s_axi_cordic_if_RREADY : IN STD_LOGIC;
      aclk : IN STD_LOGIC;
      aresetn : IN STD_LOGIC
    );
  END COMPONENT cordiccc_top;
  ATTRIBUTE X_CORE_INFO : STRING;
  ATTRIBUTE X_CORE_INFO OF uebung08_cordiccc_top_0_0_arch: ARCHITECTURE IS "cordiccc_top,Vivado 2019.1";
  ATTRIBUTE CHECK_LICENSE_TYPE : STRING;
  ATTRIBUTE CHECK_LICENSE_TYPE OF uebung08_cordiccc_top_0_0_arch : ARCHITECTURE IS "uebung08_cordiccc_top_0_0,cordiccc_top,{}";
  ATTRIBUTE CORE_GENERATION_INFO : STRING;
  ATTRIBUTE CORE_GENERATION_INFO OF uebung08_cordiccc_top_0_0_arch: ARCHITECTURE IS "uebung08_cordiccc_top_0_0,cordiccc_top,{x_ipProduct=Vivado 2019.1,x_ipVendor=xilinx.com,x_ipLibrary=hls,x_ipName=cordiccc_top,x_ipVersion=1.0,x_ipCoreRevision=2007070940,x_ipLanguage=VERILOG,x_ipSimLanguage=MIXED,C_S_AXI_CORDIC_IF_ADDR_WIDTH=6,C_S_AXI_CORDIC_IF_DATA_WIDTH=32}";
  ATTRIBUTE IP_DEFINITION_SOURCE : STRING;
  ATTRIBUTE IP_DEFINITION_SOURCE OF uebung08_cordiccc_top_0_0_arch: ARCHITECTURE IS "HLS";
  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER OF aresetn: SIGNAL IS "XIL_INTERFACENAME aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF aresetn: SIGNAL IS "xilinx.com:signal:reset:1.0 aresetn RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF aclk: SIGNAL IS "XIL_INTERFACENAME aclk, ASSOCIATED_BUSIF S_AXI_CORDIC_IF, ASSOCIATED_RESET aresetn, FREQ_HZ 50000000, PHASE 0.000, CLK_DOMAIN uebung08_processing_system7_0_2_FCLK_CLK0, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF aclk: SIGNAL IS "xilinx.com:signal:clock:1.0 aclk CLK";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_cordic_if_RREADY: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_CORDIC_IF RREADY";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_cordic_if_RVALID: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_CORDIC_IF RVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_cordic_if_RRESP: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_CORDIC_IF RRESP";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_cordic_if_RDATA: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_CORDIC_IF RDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_cordic_if_ARREADY: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_CORDIC_IF ARREADY";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_cordic_if_ARVALID: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_CORDIC_IF ARVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_cordic_if_ARADDR: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_CORDIC_IF ARADDR";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_cordic_if_BREADY: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_CORDIC_IF BREADY";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_cordic_if_BVALID: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_CORDIC_IF BVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_cordic_if_BRESP: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_CORDIC_IF BRESP";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_cordic_if_WREADY: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_CORDIC_IF WREADY";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_cordic_if_WVALID: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_CORDIC_IF WVALID";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_cordic_if_WSTRB: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_CORDIC_IF WSTRB";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_cordic_if_WDATA: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_CORDIC_IF WDATA";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_cordic_if_AWREADY: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_CORDIC_IF AWREADY";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_cordic_if_AWVALID: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_CORDIC_IF AWVALID";
  ATTRIBUTE X_INTERFACE_PARAMETER OF s_axi_cordic_if_AWADDR: SIGNAL IS "XIL_INTERFACENAME S_AXI_CORDIC_IF, ADDR_WIDTH 6, DATA_WIDTH 32, PROTOCOL AXI4LITE, READ_WRITE_MODE READ_WRITE, FREQ_HZ 50000000, ID_WIDTH 0, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 8, NUM_WRITE_OUTSTANDING 8, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN uebung08_processing_system7_0_2_FCLK_CLK0, NUM_REA" & 
"D_THREADS 4, NUM_WRITE_THREADS 4, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  ATTRIBUTE X_INTERFACE_INFO OF s_axi_cordic_if_AWADDR: SIGNAL IS "xilinx.com:interface:aximm:1.0 S_AXI_CORDIC_IF AWADDR";
BEGIN
  U0 : cordiccc_top
    GENERIC MAP (
      C_S_AXI_CORDIC_IF_ADDR_WIDTH => 6,
      C_S_AXI_CORDIC_IF_DATA_WIDTH => 32
    )
    PORT MAP (
      s_axi_cordic_if_AWADDR => s_axi_cordic_if_AWADDR,
      s_axi_cordic_if_AWVALID => s_axi_cordic_if_AWVALID,
      s_axi_cordic_if_AWREADY => s_axi_cordic_if_AWREADY,
      s_axi_cordic_if_WDATA => s_axi_cordic_if_WDATA,
      s_axi_cordic_if_WSTRB => s_axi_cordic_if_WSTRB,
      s_axi_cordic_if_WVALID => s_axi_cordic_if_WVALID,
      s_axi_cordic_if_WREADY => s_axi_cordic_if_WREADY,
      s_axi_cordic_if_BRESP => s_axi_cordic_if_BRESP,
      s_axi_cordic_if_BVALID => s_axi_cordic_if_BVALID,
      s_axi_cordic_if_BREADY => s_axi_cordic_if_BREADY,
      s_axi_cordic_if_ARADDR => s_axi_cordic_if_ARADDR,
      s_axi_cordic_if_ARVALID => s_axi_cordic_if_ARVALID,
      s_axi_cordic_if_ARREADY => s_axi_cordic_if_ARREADY,
      s_axi_cordic_if_RDATA => s_axi_cordic_if_RDATA,
      s_axi_cordic_if_RRESP => s_axi_cordic_if_RRESP,
      s_axi_cordic_if_RVALID => s_axi_cordic_if_RVALID,
      s_axi_cordic_if_RREADY => s_axi_cordic_if_RREADY,
      aclk => aclk,
      aresetn => aresetn
    );
END uebung08_cordiccc_top_0_0_arch;
