# ==============================================================
# Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2019.1 (64-bit)
# Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
# ==============================================================
proc generate {drv_handle} {
    xdefine_include_file $drv_handle "xparameters.h" "XCordiccc" \
        "NUM_INSTANCES" \
        "DEVICE_ID" \
        "C_S_AXI_CORDIC_IF_BASEADDR" \
        "C_S_AXI_CORDIC_IF_HIGHADDR"

    xdefine_config_file $drv_handle "xcordiccc_g.c" "XCordiccc" \
        "DEVICE_ID" \
        "C_S_AXI_CORDIC_IF_BASEADDR"

    xdefine_canonical_xpars $drv_handle "xparameters.h" "XCordiccc" \
        "DEVICE_ID" \
        "C_S_AXI_CORDIC_IF_BASEADDR" \
        "C_S_AXI_CORDIC_IF_HIGHADDR"
}

