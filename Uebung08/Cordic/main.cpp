#include "mySystemc.h"

using namespace sc_core;
using namespace sc_dt;
using namespace std;

#include "top.h"

int sc_main(int argc, char* agrv[]) {
	sc_trace_file* myTraceFile;
	myTraceFile = sc_create_vcd_trace_file("cordic_trace");
	myTraceFile->delta_cycles(true);

	top top("top");

	sc_trace(myTraceFile, top.mCordicTarget->Start, "iStart");
	sc_trace(myTraceFile, top.mCordicTarget->Rdy, "oRdy");

	sc_start();

	sc_close_vcd_trace_file(myTraceFile);
	return 0;
}
