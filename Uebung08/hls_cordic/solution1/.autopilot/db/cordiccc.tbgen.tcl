set moduleName cordiccc
set isTopModule 1
set isTaskLevelControl 1
set isCombinational 0
set isDatapathOnly 0
set isFreeRunPipelineModule 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set ProfileFlag 0
set StallSigGenFlag 0
set isEnableWaveformDebug 1
set C_modelName {cordiccc::cordiccc}
set C_modelType { void 0 }
set C_modelArgList {
	{ clk int 1 unused {pointer 0}  }
	{ rst int 1 unused {pointer 0}  }
	{ iStart int 1 regular {pointer 0 volatile }  }
	{ oRdy int 1 regular {pointer 1 volatile }  }
	{ iPhi int 22 regular {pointer 0 volatile }  }
	{ oX int 16 regular {pointer 1 volatile }  }
	{ oY int 16 regular {pointer 1 volatile }  }
}
set C_modelArgMapList {[ 
	{ "Name" : "clk", "interface" : "wire", "bitwidth" : 1, "direction" : "READONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "cordiccc.clk.m_if.Val","cData": "bool","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "rst", "interface" : "wire", "bitwidth" : 1, "direction" : "READONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "cordiccc.rst.m_if.Val","cData": "bool","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "iStart", "interface" : "wire", "bitwidth" : 1, "direction" : "READONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "cordiccc.iStart.m_if.Val","cData": "bool","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "oRdy", "interface" : "wire", "bitwidth" : 1, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":0,"cElement": [{"cName": "cordiccc.oRdy.m_if.Val","cData": "bool","bit_use": { "low": 0,"up": 0},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "iPhi", "interface" : "wire", "bitwidth" : 22, "direction" : "READONLY", "bitSlice":[{"low":0,"up":21,"cElement": [{"cName": "cordiccc.iPhi.m_if.Val.V","cData": "uint22","bit_use": { "low": 0,"up": 21},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "oX", "interface" : "wire", "bitwidth" : 16, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":15,"cElement": [{"cName": "cordiccc.oX.m_if.Val.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "oY", "interface" : "wire", "bitwidth" : 16, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":15,"cElement": [{"cName": "cordiccc.oY.m_if.Val.V","cData": "uint16","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} ]}
# RTL Port declarations: 
set portNum 7
set portList { 
	{ clk sc_in sc_logic 1 clock -1 } 
	{ rst sc_in sc_logic 1 reset -1 active_high_sync clk } 
	{ iStart sc_in sc_logic 1 signal 2 clk } 
	{ oRdy sc_out sc_logic 1 signal 3 clk } 
	{ iPhi sc_in sc_lv 22 signal 4 clk } 
	{ oX sc_out sc_lv 16 signal 5 clk } 
	{ oY sc_out sc_lv 16 signal 6 clk } 
}
set NewPortList {[ 
	{ "name": "clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "clk", "role": "default" }} , 
 	{ "name": "rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "rst", "role": "default" }} , 
 	{ "name": "iStart", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "iStart", "role": "default" }} , 
 	{ "name": "oRdy", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "oRdy", "role": "default" }} , 
 	{ "name": "iPhi", "direction": "in", "datatype": "sc_lv", "bitwidth":22, "type": "signal", "bundle":{"name": "iPhi", "role": "default" }} , 
 	{ "name": "oX", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "oX", "role": "default" }} , 
 	{ "name": "oY", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "oY", "role": "default" }}  ]}

set RtlHierarchyInfo {[
	{"ID" : "0", "Level" : "0", "Path" : "`AUTOTB_DUT_INST", "Parent" : "", "Child" : ["1"],
		"CDFG" : "cordiccc",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "0", "ap_start" : "0", "ap_ready" : "0", "ap_done" : "0", "ap_continue" : "0", "ap_idle" : "0",
		"Pipeline" : "Dataflow", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "1",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "0", "EstimateLatencyMax" : "138",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"InputProcess" : [],
		"OutputProcess" : [],
		"Port" : [
			{"Name" : "clk", "Type" : "None", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "grp_cordiccc_doWork_fu_72", "Port" : "clk"}]},
			{"Name" : "rst", "Type" : "None", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "grp_cordiccc_doWork_fu_72", "Port" : "rst"}]},
			{"Name" : "iStart", "Type" : "None", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "grp_cordiccc_doWork_fu_72", "Port" : "iStart"}]},
			{"Name" : "oRdy", "Type" : "Vld", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "grp_cordiccc_doWork_fu_72", "Port" : "oRdy"}]},
			{"Name" : "iPhi", "Type" : "None", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "grp_cordiccc_doWork_fu_72", "Port" : "iPhi"}]},
			{"Name" : "oX", "Type" : "Vld", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "grp_cordiccc_doWork_fu_72", "Port" : "oX"}]},
			{"Name" : "oY", "Type" : "Vld", "Direction" : "O",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "grp_cordiccc_doWork_fu_72", "Port" : "oY"}]},
			{"Name" : "cordiccc_ssdm_thread_M_doWork", "Type" : "None", "Direction" : "I"},
			{"Name" : "atanArray_V", "Type" : "Memory", "Direction" : "I",
				"SubConnect" : [
					{"ID" : "1", "SubInstance" : "grp_cordiccc_doWork_fu_72", "Port" : "atanArray_V"}]}]},
	{"ID" : "1", "Level" : "1", "Path" : "`AUTOTB_DUT_INST.grp_cordiccc_doWork_fu_72", "Parent" : "0", "Child" : ["2", "3", "4"],
		"CDFG" : "cordiccc_doWork",
		"Protocol" : "ap_ctrl_hs",
		"ControlExist" : "0", "ap_start" : "0", "ap_ready" : "0", "ap_done" : "0", "ap_continue" : "0", "ap_idle" : "0",
		"Pipeline" : "None", "UnalignedPipeline" : "0", "RewindPipeline" : "0", "ProcessNetwork" : "0",
		"II" : "0",
		"VariableLatency" : "1", "ExactLatency" : "-1", "EstimateLatencyMin" : "137", "EstimateLatencyMax" : "137",
		"Combinational" : "0",
		"Datapath" : "0",
		"ClockEnable" : "0",
		"HasSubDataflow" : "0",
		"InDataflowNetwork" : "0",
		"HasNonBlockingOperation" : "0",
		"Port" : [
			{"Name" : "clk", "Type" : "None", "Direction" : "I"},
			{"Name" : "rst", "Type" : "None", "Direction" : "I"},
			{"Name" : "iStart", "Type" : "None", "Direction" : "I"},
			{"Name" : "oRdy", "Type" : "Vld", "Direction" : "O"},
			{"Name" : "iPhi", "Type" : "None", "Direction" : "I"},
			{"Name" : "oX", "Type" : "Vld", "Direction" : "O"},
			{"Name" : "oY", "Type" : "Vld", "Direction" : "O"},
			{"Name" : "atanArray_V", "Type" : "Memory", "Direction" : "I"}]},
	{"ID" : "2", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cordiccc_doWork_fu_72.atanArray_V_U", "Parent" : "1"},
	{"ID" : "3", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cordiccc_doWork_fu_72.cordiccc_mul_mul_cud_U1", "Parent" : "1"},
	{"ID" : "4", "Level" : "2", "Path" : "`AUTOTB_DUT_INST.grp_cordiccc_doWork_fu_72.cordiccc_mul_mul_cud_U2", "Parent" : "1"}]}


set ArgLastReadFirstWriteLatency {
	cordiccc {
		clk {Type I LastRead -1 FirstWrite -1}
		rst {Type I LastRead -1 FirstWrite -1}
		iStart {Type I LastRead 2 FirstWrite -1}
		oRdy {Type O LastRead -1 FirstWrite 0}
		iPhi {Type I LastRead 2 FirstWrite -1}
		oX {Type O LastRead -1 FirstWrite 6}
		oY {Type O LastRead -1 FirstWrite 8}
		cordiccc_ssdm_thread_M_doWork {Type I LastRead -1 FirstWrite -1}
		atanArray_V {Type I LastRead -1 FirstWrite -1}}
	cordiccc_doWork {
		clk {Type I LastRead -1 FirstWrite -1}
		rst {Type I LastRead -1 FirstWrite -1}
		iStart {Type I LastRead 2 FirstWrite -1}
		oRdy {Type O LastRead -1 FirstWrite 0}
		iPhi {Type I LastRead 2 FirstWrite -1}
		oX {Type O LastRead -1 FirstWrite 6}
		oY {Type O LastRead -1 FirstWrite 8}
		atanArray_V {Type I LastRead -1 FirstWrite -1}}}

set hasDtUnsupportedChannel 0

set PerformanceInfo {[
	{"Name" : "Latency", "Min" : "0", "Max" : "138"}
	, {"Name" : "Interval", "Min" : "1", "Max" : "139"}
]}

set PipelineEnableSignalInfo {[
]}

set Spec2ImplPortList { 
	iStart { ap_none {  { iStart in_data 0 1 } } }
	oRdy { ap_vld {  { oRdy out_data 1 1 } } }
	iPhi { ap_none {  { iPhi in_data 0 22 } } }
	oX { ap_vld {  { oX out_data 1 16 } } }
	oY { ap_vld {  { oY out_data 1 16 } } }
}

set busDeadlockParameterList { 
}

# RTL port scheduling information:
set fifoSchedulingInfoList { 
}

# RTL bus port read request latency information:
set busReadReqLatencyList { 
}

# RTL bus port write response latency information:
set busWriteResLatencyList { 
}

# RTL array port load latency information:
set memoryLoadLatencyList { 
}
