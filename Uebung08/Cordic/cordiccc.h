#pragma once

#include "mySystemc.h"

#define cordic_in sc_ufixed<22, 1>
#define cordic_out sc_ufixed<16, 0, SC_RND, SC_SAT>

SC_MODULE(cordiccc)
{
	sc_in<bool> clk;
	sc_in<bool> rst;
	sc_in<bool> iStart;
	sc_out<bool> oRdy;
	sc_in<cordic_in > iPhi;
	sc_out<cordic_out > oX;
	sc_out<cordic_out > oY;

	SC_CTOR(cordiccc)
	{
		SC_CTHREAD(doWork, clk.pos());
		reset_signal_is(rst, true);
	}

private:
	void doWork();
};
