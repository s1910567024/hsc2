#include "mySystemc.h"
#include "Testbench.h"
#include "Cordic.h"

int sc_main(int argc, char** argv)
{
	sc_signal<bool> Start;
	sc_signal<bool> Rdy;
	sc_signal<sc_ufixed<22, 1>> Phi;
	sc_signal<sc_ufixed<16, 0, SC_RND, SC_SAT>> X;
	sc_signal<sc_ufixed<16, 0, SC_RND, SC_SAT>> Y;

	Testbench* pT = new Testbench("Testbench");
	Cordic* pC = new Cordic("Cordic");

	pT->oStart(Start);
	pT->iRdy(Rdy);
	pT->oPhi(Phi);
	pT->iX(X);
	pT->iY(Y);

	pC->iStart(Start);
	pC->oRdy(Rdy);
	pC->iPhi(Phi);
	pC->oX(X);
	pC->oY(Y);
	
	sc_trace_file * myTraceFile;
	myTraceFile = sc_create_vcd_trace_file("cordic_trace");
	myTraceFile->delta_cycles(true);

	sc_trace(myTraceFile, pC->iStart, "iStart");
	sc_trace(myTraceFile, pC->oRdy, "oRdy");
	sc_trace(myTraceFile, pC->iPhi, "iPhi");
	sc_trace(myTraceFile, pC->oX, "oX");
	sc_trace(myTraceFile, pC->oY, "oY");

	sc_start(150, SC_NS);

	sc_close_vcd_trace_file(myTraceFile);

	return 0;
}