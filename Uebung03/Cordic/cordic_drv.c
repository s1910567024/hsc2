#include "cordic_drv.h"
#include "hal.h"
#include <math.h>

#define M_PI 3.14159265358979323846

void CordicCalcXY(float const* const phi, float* const cos, float* const sin, unsigned int* adr)
{
	float bPhi = fmod(*phi, 2*M_PI); //Between -2pi and 2*pi
	if (bPhi < 0)
		bPhi += 2 * M_PI;//Between 0 and 2*pi

	unsigned int data = (unsigned int)(fmod(bPhi, M_PI/2) * (1<<21));

	CordicWrPhi(*adr, data);

	do
	{
		data = CordicRdCtl(*adr);
	} while ((data & 1) == 0);

	data = CordicRdXY(*adr);

	*cos = (data >> 16) * 1.0 / (1 << 16);
	*sin = (data & ((1 << 16) - 1)) * 1.0 / (1 << 16);

	if(bPhi < M_PI / 2) //1.Quadrant
	{
	}
	else if(bPhi < 2* M_PI / 2) //2.Quadrant
	{
		float tmp = *cos;
		*cos = -*sin;
		*sin = tmp;
	}
	else if (bPhi < 3 * M_PI / 2) //3.Quadrant
	{
		*cos = -*cos;
		*sin = -*sin;
	}
	else //4.Quadrant
	{
		float tmp = *sin;
		*sin = -*cos;
		*cos = tmp;
	}
}