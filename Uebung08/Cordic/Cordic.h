#pragma once

#include "mySystemc.h"

SC_MODULE(Cordic)
{
	sc_in<bool> iStart;
	sc_out<bool> oRdy;
	sc_in<sc_ufixed<22, 1>> iPhi;
	sc_out<sc_ufixed<16, 0, SC_RND, SC_SAT>> oX;
	sc_out<sc_ufixed<16, 0, SC_RND, SC_SAT>> oY;

	SC_CTOR(Cordic)
	{
		SC_THREAD(doWork);
	}

private:
	void doWork();
};