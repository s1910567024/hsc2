#include "hal.h"
#include "offsets.h"
#include <stdbool.h>

#include "xcordiccc.h"

extern void write_bus(unsigned int adr, unsigned int data);
extern void read_bus(unsigned int adr, unsigned int * data);
extern void wait_irq();

unsigned int CordicRdCtl(unsigned int adr)
{
	unsigned int data;
	read_bus(adr + OFFSET_CTL, &data);
	return data;
}

void CordicWrPhi(unsigned int adr, unsigned int data)
{
	write_bus(adr + OFFSET_PHI, data);
}

unsigned int CordicRdXY(unsigned int adr)
{
	unsigned int data;
	read_bus(adr + OFFSET_XY, &data);
	return data;
}

#ifdef EMUCPUINSTANCE
void WaitForIrq()
{
	wait_irq();
}
#endif
